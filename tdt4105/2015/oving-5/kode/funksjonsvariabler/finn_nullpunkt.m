function resultat = finn_nullpunkt(f, df)
    x = 1;
    while for_langt_unna_null(f(x))
        x = x - f(x) / df(x);
    end
    resultat = x;
end

function resultat = for_langt_unna_null(y)
    resultat = abs(y) >= 1e-6;
end