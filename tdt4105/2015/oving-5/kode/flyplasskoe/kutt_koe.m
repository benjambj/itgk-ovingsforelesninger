function ny_koe = kutt_koe(koe)
    [m, n] = size(koe);
    L = koe(:);
    [~, ordning] = sort(L);
    best_tid = ordning(n+1:end); % De n første har dårligst tid.
    ny_L = L(sort(best_tid));    % Hent ut de med best tid i
                                 % rekkefølge
    ny_koe = reshape(ny_L, m, n-1);
end