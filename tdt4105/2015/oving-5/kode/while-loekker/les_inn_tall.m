function tall = les_inn_tall()
    streng = input('Skriv inn et tall: ', 's');
    tall = str2double(streng);
    while isnan(tall)
        fprintf('%s er ikke et tall!\n', streng);
        streng = input('Skriv inn et tall: ', 's');
        tall = str2double(streng);
    end
end
