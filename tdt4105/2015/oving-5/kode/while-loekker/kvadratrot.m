function rot = kvadratrot(n)
    x = n / 2;
    i = 1;
    while ikke_noeyaktig_nok(x, n, 1e-6)
        old_x = x;
        x = old_x - (old_x^2 - n)/(2 * old_x);
        fprintf('Iterasjon #%d: x_%d = %d, x_%d = %d\n', ...
                i, i-1, old_x, i, x);
        i = i + 1;
    end
    rot = x;
end

function resultat = ikke_noeyaktig_nok(old_x, n, toleranse)
    resultat = abs(old_x^2 - n) >= toleranse;
end