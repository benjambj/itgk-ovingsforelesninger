function test_preallokering()
    antall_elementer = 1e6; % 1e6 = 1*10^6 = 1 million
    fprintf('Tar tiden p� � lage en liste med %d elementer i while-l�kke med preallokering\n', antall_elementer)
    tic;
    lag_liste_med_dynamisk_doblende_preallokering(antall_elementer);
    toc;
    fprintf('Tar tiden p� � lage en liste med %d elementer i while-l�kke uten preallokering\n', antall_elementer)
    tic;
    lag_liste_uten_preallokering(antall_elementer);
    toc;
    fprintf('Tar tiden p� � lage en liste med dobbelt s� mange elementer preallokert\n');
    tic;
    lag_liste_med_overdreven_preallokering(antall_elementer);
    toc;
end

function liste = lag_liste_med_dynamisk_doblende_preallokering(antall_elementer)
    kapasitet = 5;
    liste{kapasitet} = '';
    i = 0;
    while i < antall_elementer
        if i > kapasitet
            liste{kapasitet*2} = '';
            kapasitet = kapasitet*2;
        end
        liste{i+1} = 'teststreng';
        i = i + 1;
    end
    liste = liste(1:i);
end

function liste = lag_liste_uten_preallokering(antall_elementer)
    i = 0;
    while i < antall_elementer
        liste{i+1} = 'teststreng';
        i = i + 1;
    end
end

function liste = lag_liste_med_overdreven_preallokering(antall_elementer)
    liste{antall_elementer*2} = '';
    i = 0;
    while i < antall_elementer
        liste{i + 1} = 'teststreng';
        i = i + 1;
    end
    liste = liste(1:i);
end