function grenser = pulsSoneGrenser(maksPuls)
    grenser = [0.6, 0.725, 0.825, 0.875, 0.925] * maksPuls;
end