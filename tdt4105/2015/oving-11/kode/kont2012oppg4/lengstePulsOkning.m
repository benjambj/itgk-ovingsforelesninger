function [intervallLengde, intervallStart] = lengstePulsOkning(pulsData)
%% Vektorisert implementasjon
% Se øvingsforelesningsfoil for forklaring.
    diffs = pulsData(2:end) - pulsData(1:end-1);
    fall = find(diffs < 0);
    intervallEnder = [0, fall, length(pulsData)];
    intervall = intervallEnder(2:end) - intervallEnder(1:end-1);
    [intervallLengde, intervallIndeks] = max(intervall);
    intervallStart = intervallEnder(intervallIndeks) + 1;
    
%% Alternativ implementasjon
%     intervallLengde = 1;
%     intervallStart = 1;
%     maksIntervallLengde = 0;
%     maksIntervallStart = 0;
%     for i = 2:length(pulsData)
%        diff = pulsData(i) - pulsData(i-1);
%        if diff >= 0
%            intervallLengde = intervallLengde + 1;
%            if intervallLengde > maksIntervallLengde
%                maksIntervallLengde = intervallLengde;
%                maksIntervallStart = intervallStart;
%            end
%        else
%            intervallStart = i;
%            intervallLengde = 1;
%        end
%     end
%     intervallStart = maksIntervallStart;
%     intervallLengde = maksIntervallLengde;
end