%% Demonstrerer preallokering og indeksering med logiske vektorer
% I oppgaven skal vi ta inn en celleliste, og returnere en ny celleliste
% som inneholder ordene fra den f�rste listen med engelske vokaler fjernet.
% 
% Siden vi skal returnere en liste, b�r vi vurdere muligheten for
% preallokering. Vi skal returnere like mange ord, s� lengden av
% resultatlisten er gitt fra lengden av input-listen. Vi vet ogs� at
% resultatet skal v�re en celleliste. Vi kan derfor preallokere
% resultatet vha cell-funksjonen, som lager en celleliste med oppgitte
% dimensjoner.
%
% N�r vi s� fyller ut listen, �nsker vi � lage ord uten vokalene inkludert.
% Dette kan vi f� til ved � filtrere ut de bokstavene som ikke er vokaler
% fra det opprinnelige ordet. Filtrering av vektorer, som strenger jo er,
% kan vi gj�re vha indeksering med logiske vektorer. Den logiske vektoren
% m� v�re like lang som vektoren som indekseres, og elementene i strengen
% hvor filterverdien p� samme plass i den logiske vektoren er 'true' vil
% bli tatt med.
% Eksempel:
%         ord = [  'h',   'o',   'u',  's',   'e' ]
%      filter = [ true, false, false, true, false ]
% --------------------------------------------------
% ord(filter) = [ 'h', 's' ]
% Hvis vi lager et filter som beskriver hvilke element som ikke er vokal,
% kan vi s� hente ut disse ved direkte indeksering.
function newList = noVowels(list)
    newList = cell(1, length(list));
    for i = 1:length(list)
        %% L�sning delt i flere sm� steg
        % smaaBokstaver = list{i};
        % smaaVokaler = 'aeiouy';
        % er_vokal_filter = ismember(smaaBokstaver, smaaVokaler)
        % er_ikke_vokal_filter = ~er_vokal_filter
        % newList{i} = list{i}(er_ikke_vokal_filter)
        %% Den samme l�sningen skrevet p� �n linje
        newList{i} = list{i}(~ismember(lower(list{i}), 'aeiouy'));
    end
end