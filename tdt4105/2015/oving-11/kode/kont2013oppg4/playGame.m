function numCorrect = playGame(answers, puzzles)
    numCorrect = 0;
    for i = 1:length(answers)
        fprintf('Puzzle word: %s\n', puzzles{i});
        guess = input('Guess word? ', 's');
        if strcmp(guess, answers{i})
            disp('You answered correctly!');
            numCorrect = numCorrect + 1;
        else
            fprintf('You answered incorrectly! The answer should be %s\n', answers{i});
        end
    end
end