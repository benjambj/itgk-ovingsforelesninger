%% Demonstrerer cellelister, og notat om preallokering i while-l�kker.
% Vi skal konstruere en liste av ord. 
% Siden ordene kan v�re av forskjellig lengde, bruker vi en celleliste
% (les i boka, og se figur i �vingsforelesning nr. 7).
% Lengden p� listen er bestemt av antall ord brukeren vil skrive inn,
% og vi vet dermed ikke st�rrelsen p� listen p� forh�nd. I disse tilfellene
% er det beste � allokere en liste vi definitivt tror er stor nok, doble
% kapasiteten p� listen hver gang den overskrides, og til slutt bare ta med
% elementene vi faktisk har brukt n�r vi returnerer fra funksjonen.
% 
% Hastighet er imidlertid ikke viktig i denne oppgaven, siden matlab-koden
% v�r uansett er mange ganger raskere enn brukeren som skal skrive inn
% forskjellige ord. Vi kan derfor like gjerne holde v�r kode enkel, og
% ignorere preallokering her.
function wordList = enterWords()
    i = 1;
    ord = input('Enter word [Press Enter to quit]: ', 's');
    while ~isempty(ord)
       wordList{i} = ord;
       i = i + 1;
       ord = input('Enter word [Press Enter to quit]: ', 's');
    end
end