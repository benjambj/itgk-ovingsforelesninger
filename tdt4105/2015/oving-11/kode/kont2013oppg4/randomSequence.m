%% Demonstrerer direkte konstruksjon, og indeksering med posisjonsvektor.
% I oppgaven skal vi ta to lister, og returnere de to listene stokket om p�
% samme vis.
%
% Listene skal stokkes om p� samme vis, hvilket betyr at vi �nsker at
% elementene p� plass 'i' i de opprinnelige vektorene skal v�re p� samme
% plass i de resulterene vektorene. Vi vil ogs� ha med alle element. Vi
% trenger derfor en permutasjon, alts� en vilk�rlig ordnet liste av tallene
% fra og med 1 til og med lengden av listene. Gitt lister av lengde 4, kan
% en eksempelpermutasjon v�re [1, 4, 2, 3]. Det g�r an � la permutasjonen
% v�re implisitt gitt av hvordan vi til slutt ender opp med � stokke
% vektoren, men hvis vi lager en gitt permutasjon og bruker indeksering i
% hver liste vil vi f� det resultatet vi �nsker oss direkte. 
% Eksempel:
%              liste = { 'en', 'god', 'vin', 'r�d' }
%        permutasjon = [    1,     4,     2,     3 ]
% --------------------------------------------
% liste(permutasjon) = { 'en', 'r�d', 'god', 'vin' }
%
% Siden vi skal returnere to lister, b�r vi igjen vurdere preallokering.
% Siden indekseringen gir oss resultatlisten vi �nsker oss direkte, trenger
% vi imidlertid ikke preallokere eksplisitt. Vi konstruerer hele listen
% direkte (med en gang), og matlab h�ndterer da allokering av nok minne.
function [newListOne, newListTwo] = randomSequence(listOne, listTwo)
    permutation = generatePermutation(length(listOne))
    newListOne = listOne(permutation);
    newListTwo = listTwo(permutation);
end

% Denne funksjonen genererer en permutasjon vi kan brukere for � stokke om
% p� listene. Her skal vi returnere en liste av tall - og her preallokerer
% vi, siden vi vet hvor mange listelement vi skal ha og ikke vet om noen
% m�te � generere en slik permutasjon direkte.
%  (med mindre vi vet om matlab-funksjonen randperm...)
%
% Koden fungerer ved � starte med � sette resultatet til � v�re en
% liste med alle tallene vi skal ha med (1:n => [1, 2, 3, ... ,n]).
% Deretter g�r vi gjennom listen bakfra, velger et tilfeldig tall fra
% start til det vi ser p�, og bytter plass mellom det tilfeldig trukne
% tallet og det vi n� ser p�. Slik stokkes listen gradvis bakfra
% (som tilsvarer trekk av tall uten tilbaketrekning).
%
% Eksempel: kj�ring av generatePermutation(5) (utropstegnet (!) i
% listen 'resultat' markerer et skille mellom ferdig stokkede element
% (til h�yre) og ustokkede element (til venstre)
%    i |  r | resultat         
%  ----+----+------------------
%      |    | [ 1, 2, 3, 4, 5!] 
%    5 |  3 | [ 1, 2, 5, 4! 3 ]
%    4 |  1 | [ 4, 2, 5! 1, 3 ]
%    3 |  3 | [ 5, 2! 4, 1, 3 ]
%    2 |  2 | [ 2! 5, 4, 1, 3 ]
% (avslutter ved i = 2, siden det bare er ett ustokket element igjen).
%
% Denne stokke-m�ten kalles b�de "Fisher-Yates shuffle" og "Knuth
% shuffle" (https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle).
function resultat = generatePermutation(n)
    resultat = 1:n;
    for i = n:-1:2
        r = randi(i);
        % Bytt plass p� tall nummer 'r' og tall nummer 'i'.
        % Dette tilsvarer at vi har 'trukket' tall nr. 'r'.
        t = resultat(r);
        resultat(r) = resultat(i);
        resultat(i) = t;
    end
end