%% Demonstrasjonsmal
%% Denne filen inneholder kodeeksempler som er planlagt gjennomgått
% som løpende eksempler gjennom timen. Den illustrerer forskjellige
% aspekt ved teknikkene. Løsning på eksempeloppgaver finnes i egne
% filer.

%% Eksisterende funksjoner (innputt og utputt)

% Operatorer kan gi oss enkle beregninger.
x = 1 + 2;
y = 2 * 2 - 3 ^ x;
% I første eksempel over regner operatoren '+' ut resultatet av å
% addere de to operandene 1 og 2. 

% Funksjoner kan gjøre mer kompliserte beregninger. Når man snakker om
% funksjoner, bruker man ordet 'funksjon' i stedet for 'operator' og
% 'parameter' eller 'argument' i stedet for 'operand'. Det er likevel
% snakk om relativt like konsept: funksjonen angir hva slags beregning
% som skal gjennomføres, og parameterene angir hvilke data
% beregningen skal gjøres på. La oss se på et eksempel, hvor vi
% bruker den innebygde funksjonen 'sum':

vektor = [1, 2, 3];
summen = sum(vektor); 
% Funksjonskall ser ut som oppslag i en vektor. Her bruker vi
% funksjonen 'sum', med variabelen 'vektor' som argument. Funksjonen
% regner ut verdien av alle vektorelementene summert = 1 + 2 + 3 =
% 6. Dette svaret lagrer vi i variabelen 'summen'.

lengden = length(vektor); 
% Å bruke en funksjon omtales gjerne som å 'kalle funksjonen'. Her
% kaller vi altså funksjonen 'length', med variabelen 'vektor' som
% argument. Funksjonens resultat er lengden på vektoren, altså 3.

% Det er verdt å nevne på dette tidspunktet at det er en liten
% forskjell på hvordan vi bruker ordet 'parameter' og 'argument'. Det
% gir imidlertid ikke mening før vi ser på hvordan vi lager våre
% egne funksjoner, så inntil videre kan dere tenke på 'parameter'
% og 'argument' som to ord for funksjonens 'operander'.

% I motsetning til operatorer, er det ikke alle funksjoner som regner
% ut et svar. Et eksempel på dette er den innebygde funksjonen
% 'disp'. "Beregningen" som 'disp' gjør er å skrive verdien av sin ene
% parameter til konsollvinduet. I eksemplene under kaller vi
% funksjonen 'disp' to ganger, først med variabelen 'summen' som
% argument og deretter med variabelen 'lengden'.
disp(summen); % Skriver ut 6.
disp(lengden); % Skriver ut 3.

% 'disp' er en typisk utputt-funksjon, som vi kan bruke til å
% rapportere det Matlab regner ut til en bruker. Et eksempel på en
% innputt-funksjon, som åpner for kommunikasjon andre veien ved å la
% brukeren mate Matlab med data, er 'input'. Artig nok produserer
% 'input' først litt utputt ved å skrive ut sin ene parameter til
% skjerm, før den leser hva brukeren skriver. Det brukeren skriver, er
% resultatet fra funksjonen.
tall = input('Skriv inn et tall: ');
% Hvis brukeren skriver inn 42, vil resultatet av å kalle
% funksjonen 'input' bli 42. I variabeltilordningen over vil vi
% derfor binde navnet 'tall' til verdien 42. 
%
% Noe artig verdt å merke seg om innputt, er at det brukeren skriver
% inn tolkes som en matlabkommando. Hvis brukeren skriver inn
% 'summen', vil resultatet bli verdien av variabelen 'summen'.

disp(tall); %Skriver ut verdien av uttrykket brukeren skrev inn. 

% Selv om vi over har snakket om at funksjoner tar variabler som
% argument, er det viktig å være klar over at Matlab sender en kopi av
% variabelen til funksjonen. For å være presis, burde vi derfor si at
% funksjonen tar verdien av en variabel som argument: i eksempelet
% over, tar funksjonen 'disp' verdien av variabelen 'tall' som
% argument. Grunnen til at dette er viktig, er at funksjonen 'disp'
% ikke har noen mulighet til å endre verdien til variabelen
% 'tall'. Dette vil vi se siden.


%% Egne funksjoner 

% Vi har nå sett flere eksempler på hvordan man kan bruke
% funksjoner. Når man lager større matlab-program, vil det være veldig
% nyttig å også kunne lage sine egne. La oss se hvordan det gjøres.

% La oss si vi har lyst til å lage en funksjon som regner summen av to
% tall. (Dette er selvfølgelig en fullstendig unyttig funksjon siden
% Matlab har operatoren '+' for dette formålet, men det gir en god
% sammenlikning mellom operatorer/funksjoner og operander/parametere.)
% Det første man må gjøre, er å lage en ny Matlab-fil. Filen må
% hete "funksjonsnavn.m", så hvis vi ønsker å kalle funksjonen vår
% 'pluss' må vi lage en ny fil som heter "pluss.m". 

% (Selv om funksjonene egentlig må i egne filnavn, inkluderer jeg
% funksjonsdefinisjonene også i denne filen så man slipper å åpne
% mange nye filer hele tiden. Dette gjør dessverre at denne filen ikke
% er et gyldig Matlab-script (siden funksjoner ikke kan lages på
% vanlig måte i script), så hvis dere vil kjøre hele denne filen som
% et script (ved å skrive 'demonstrasjonsmal' i kommandovinduet) må
% dere først fjerne eller kommentere ut funksjonsdefinisjonene i denne
% filen. Funksjonene er også tilgjengelige som egne filer i
% kodevedlegget.)

% Alle nye funksjoner man lager, følger en kodemal. Se figur på
% foilene for en forklaring av formatet. Vår funksjon 'pluss' blir
% seende ut som følger:
function resultat = pluss(a, b)
    resultat = a + b;
end

% Når vi har laget vår nye funksjon, kan vi kalle den slik som de
% innebygde funksjonene:
x = pluss(2, 3);
% Akkurat som med de innebygde funksjonene, vil funksjonskallet føre
% til at beregningen som 'pluss' representerer blir utført. Denne
% beregningen består av all Matlab-koden som står mellom 'function' og
% 'end'. For å gjennomføre funksjonskallet, vil Matlab først lage nye
% variabler for hver funksjonsparameter og binde parameternavnene til
% verdiene til argumentene. I vårt eksempel, vil altså parameternavnet
% 'a' bindes til verdien 2 og parameternavnet 'b' bindes til verdien
% 3. Deretter vil koden på linjene mellom 'function' og 'end'
% kjøres. I vårt tilfelle er dette bare én linje, "resultat = a +
% b". Den binder variabelen 'resultat' til verdien av 'a + b', som er
% 5. Når funksjonen er ferdig, vil Matlab kunne finne resultatet ved å
% se på verdien bundet til returverdivariabelnavnet. I vårt tilfelle
% heter denne variabelen 'resultat'. Siden den variabelen har verdien
% 5 når funksjonen er ferdig, blir resultatet av funksjonen
% 5. Dette resultatet binder vi variabelnavnet 'x' til, siden vi
% kalte funksjonen i en variabeltilordning. 

disp(x);  % Skriver ut 5

% Alle variabler som opprettes i forbindelse med beregningen av en
% funksjon, er bare tilgjengelige inne i funksjonen. Vi kan derfor
% ikke se noe spor av parametervariablene eller
% resultatverdivariabelen utenfor funksjonen.
disp(a); % Gir oss en feilmelding.
disp(resultat); % Gir oss en feilmelding.

% Legg spesielt merke til at resultatverdivariabelen ikke er koblet
% på noe vis til hva variabelen vi lagrer resultatet i heter. Det
% vil ikke fungere å skrive funksjonen 'pluss' som følger:
function resultat = pluss(a, b)
    x = a + b; % Ikke riktig!
end
% Resultatet til funksjonen må lagres i resultatverdivariabelen!  Hvis
% vi prøver å evaluere uttrykket 'x = pluss(2, 3)' med denne
% definisjonen av pluss, vil Matlab klage over at ingen verdi er
% gitt til variabelen 'resultat'. 

% Resultatverdivariabelen kan hete hva som helst. Det vil altså
% fungere å skrive funksjonen som følger:
function tjolahopp = pluss(a, b)
    tjolahopp = a + b;
end
% Det er imidlertid smart å gi variabler navn som gjør at man
% skjønner hva verdiene de er bundet til representerer. 'resultat'
% er et navn som i det minste forteller oss at variabelen bindes
% til funksjonens resultat. Vi kunne også vurdert å kalle
% returverdivariabelen 'summen'.

% Nå kan vi forklare forskjellen på 'parameter' og 'argument'. Når vi
% lager våre egne funksjoner, omtaler vi funksjonens operander som
% 'parametere'. Når vi kaller en funksjon, omtaler vi funksjonens
% operander som 'argumenter'. I funksjonen 'pluss' vil vi derfor kalle
% 'a' og 'b' for funksjonens parametere, mens vi i funksjonskallet
% over vil kalle verdiene 2 og 3 for funksjonens
% argumenter. 'Argument' er altså et begrep som gjelder et
% funksjonskall, mens 'parameter' er et begrep som gjelder en
% funksjonsdefinisjon.

% Funksjoner kan ha et vilkårlig antall parametere. La oss lage en
% funksjon som inverterer et tall. 
function resultat = inverter(tall)
    resultat = 1 / tall;
end

% La oss lage en funksjon som regner ut a + b*c.
function resultat = adder_multiplikasjon(a, b, c)
    resultat = a + b * c;
end

% Funksjoner kan også ta ingen parametere. La oss prøve å lage en
% funksjon som leser inn et tall fra brukeren. 
function tall = les_inn_tall()
    tall = input('Skriv inn et tall: ');
end

% Husk at variabler kan bindes til vektorverdier:
v1 = [1, 2, 3]; % Går helt fint.  Tilsvarende kan parametere og
% resultatvariabler også bindes til vektorer. Eksempeloppgave: vi har
% en vektor med studentnumre. Vi skal dele studentene inn i grupper på
% tre, men det er ikke sikkert at antallet studenter er delelig på
% tre. Lag en funksjon som returnerer hvor mange studenter som står
% igjen hvis vi prøver å dele studentene inn i grupper på tre. 
function rest = resterende_studenter(studenter)
    rest = mod(length(studenter), 3);
end

% Eksempeloppgave: lag en funksjon som lager en vektor med som har
% en gitt startverdi, en gitt steglengde mellom elementene, og et
% gitt antall elementer.
function vektor = lag_vektor(startverdi, steglengde, antall_element)
    sluttverdi = startverdi + steglengde * (antall_element - 1);
    vektor = startverdi:steglengde:sluttverdi;
end

% Vær oppmerksomme på at parametere og resultatverdivariabler kan hete
% det samme uten at Matlab klager. Dette kan utnyttes, men det kan
% også føre til små feil. La oss se for oss at vi har en butikk hvor
% epler og bananer koster det samme, men prisen kan variere. Vi ønsker
% å skrive en Matlab-funksjon som regner ut hva det koster en kunde å
% kjøpe et visst antall epler og bananer gitt en eple/banan-pris. Med
% litt sløv navngivning av variabler, kan vi fort ende opp med å kalle
% resultatverdivariabelen og en parameter det samme. Dette kan by
% på problemer, som hvis man forsøker å implementere funksjonen som
% følger:
function pris = totalpris(antall_epler, antall_bananer, pris)
    pris = antall_epler * pris;
    % Huff huff, dette går ikke bra! Variabelen 'pris' ble nemlig
    % overskrevet i forrige linje, så vi får ikke tak i
    % argumentverdien lenger.
    pris = pris + antall_bananer * pris;
end

% Det kan imidlertid gå bra, hvis funksjonen implementeres som
% følger:
function pris = totalpris(antall_epler, antall_bananer, pris)
    pris = (antall_epler + antall_bananer) * pris;
    % Dette går bra, fordi 'pris' i utregningen har verdien til
    % argumentet. Det går bra å overskrive verdien av 'pris', siden vi
    % ikke trenger den opprinnelige verdien lenger.
end

% Man vil av og til se dette utnyttet, som regel når man skriver en
% funksjon som prøver å endre en variabel. La oss anta vi skriver et
% program som håndterer en liste med alderen til studenter, og at vi
% ønsker en funksjon som gjør det mulig å endre alderen til en gitt
% student. Når vi skal skrive denne funksjonen, må vi huske at
% argumentene til funksjonen kopieres. Følgende implementasjon vil
% derfor ikke fungere:
function endre_alder(studentaldre, student, ny_alder)
    studentaldre(student) = ny_alder;
end
% Prøver vi å bruke funksjonen som under, vil det ikke ha noen
% effekt:
studentaldre = [ 19, 21, 20 ];
endre_alder(studentaldre, 1, 20);
disp(studentaldre); % Skriver ut [ 19, 21, 20 ]
% Se foilene for en illustrasjon av hva som skjer, og hvorfor det
% ikke fungerer.

% I stedet kan vi skrive funksjonen som følger:
function studentaldre = endre_alder(studentaldre, student, ...
                                    ny_alder)
    studentaldre(student) = ny_alder;
end
% Funksjonen vil da brukes som følger:
studentaldre = [ 19, 21, 20 ];
studentaldre = endre_alder(studentaldre, 1, 20);
disp(studentaldre); % Skriver ut [ 20, 21, 20 ];

% En siste ting om funksjoner: husk at det er filnavnet som avgjør
% hva funksjonen heter. La oss si du lagrer følgende funksjon i
% filen 'f1.m':
function skriv_poengsum(poeng)
    disp('Poengsummen er:');
    disp(poeng);
end
% Følgende funksjonskall vil da fungere:
f1(100);
% Følgende funksjonskall vil ikke fungere:
skriv_poengsum(100);
% Husk derfor å kalle filnavnet det samme som funksjonsnavnet.

% Se på foilene for en illustrasjon av forskjellen på retur av verdier
% og utskrift til skjerm.

%% Matriser

%Vi har tidligere sett vektorer, som lar oss lage en-dimensjonale
%lister med tall:
rundetider = [ 30, 29, 31, 30 ];
disp(rundetider);

% Det overnevnte oppretter en rad med tall. Hvis vi ønsker det, kan
% vi la den ene dimensjonen med tall være en kolonne i stedet ved å
% bruke semikolon til å skille tallene:
rundetider = [ 30; 29; 31; 30 ];
disp(rundetider); % Skriver ut tallene under hverandre. 

% Merk at kolon-operasjonen alltid lager rader. For å lage en
% kolonne, kan man transponere raden:
oddetall_rad = 1:2:9;
oddetall_kolonne = (1:2:9)'; % Fnutten er transponeringsoperasjon,
                             % som gjør rader til kolonner.
disp(oddetall_rad);
disp(oddetall_kolonne);

% Man kan bruke den innebygde funksjonen 'size' til å se dimensjonene
% til en vektor:
disp(size(oddetall_rad)); % Skriver ut [ 1, 5 ]
disp(size(oddetall_kolonne)); % Skriver ut [ 5, 1 ]
% Det første tallet i resultatvektoren fra 'size' angir antall
% rader, og det andre tallet angir antall kolonner.

% Matriser kan man se på som en samling vektorer. Man kan lage dem
% i Matlab ved å kombinere komma og semikolon:
rundetider = [ 25, 29, 31, 30 ; ...
               25, 31, 29, 29 ; ...
               24, 30, 32, 33 ];
disp(rundetider);
% Hvert komma starter en ny kolonne, og hvert semikolon starter en ny
% rad. I matriser må alle rader ha like mange kolonner, og alle
% kolonner må ha like mange rader.

% Man kan også se på en vektor som en spesiell type matrise, hvor
% bare én dimensjon kan ha lengde større enn 1. Matriser har ingen
% slik begrensning:
disp(size(rundetider)); % Skriver ut [ 3, 4 ]

% Akkurat som vektorer, kan man hente ut enkeltelement fra
% matrisen. La oss anta at variabelen 'rundetider' peker på en
% matrise som beskriver rundetidene for tre skøyteløpere, som har
% gått fire runder hver. Vi kan hente ut tiden på tredje runde for
% skøyteløper nr. to ved å bruke et indekseringsuttrykk:
rundetid = rundetider(2, 3);
disp(rundetid); % Skriver ut 29

% Som med vektorer, skriver vi variabelnavnet, parenteser, og
% posisjonen vi vil hente en verdi fra. Siden matriser har flere
% dimensjoner med lengde > 1, må vi oppgi flere posisjoner. Disse
% skiller vi med komma. 'rundetider(2, 3)' er altså en forespørsel
% om elementet på rad 2, kolonne 3. 

% Når man indekserer matriser eller vektorer, er det ingenting i
% veien for å be om flere elementer på en gang. Gitt en vektor med
% datoer i en uke, kan man hente ut datoene for mandag, torsdag og
% fredag som følger:
datoer = [ 7, 8, 9, 10, 11, 12, 13 ];
ma_to_fr = datoer([1, 4, 5]);
 
% Helgens datoer kan man hente ut som følger:
helg = datoer([5, 6, 7]);
% Siden vektoren [ 5, 6, 7 ] kan regnes ut med kolon-operatoren,
% kan vi også skrive det som følger:
helg = datoer(5:7); % Regner først ut 5:7 = [5, 6, 7], og henter så
                    % disse elementene fra vektoren 'datoer'.

% Matriseindeksering fungerer tilsvarende, bortsett fra at vi
% trenger å spesifisere posisjoner for alle dimensjonene. Hvis vi
% ønsker å hente ut rundetid nr. 3 for skøyteløper 2 og 3, kan vi
% gjøre det som følger:
tider = rundetider([2, 3], 3); % Henter ut verdiene på rad 2 og 3
                               % og kolonne 3.
% Alternativt:
tider = rundetider(2:3, 3);

% Hva hvis vi vil hente ut rundetid nr. 2 og 3 for løper nr. 2?
tider = rundetider(2, [2, 3]); % Henter ut verdiene på rad 2 og
                               % kolonne 2 og 3.
% Alternativt: 
tider = rundetider(2, 2:3);

% Vi kan godt hente ut flere verdier fra flere dimensjoner også. Hva
% om vi ønsker rundetid 1 og 4 for løper 2 og 3? Ikke noe problem:
tider = rundetider([2, 3], [1, 4]);

% Når vi indekserer matriser, ønsker vi av og til alle elementene i
% en dimensjon. Dette kan vi indikere med tegnet kolon. For å hente
% ut alle rundetidene til skøyeløper nr. 1, kan vi skrive følgende:
tider = rundetider(1, :); % Henter ut verdiene i rad 1, i alle
                          % kolonner.
disp(tider); % Skriver ut [ 25, 29, 31, 30 ];

% For å hente ut rundetid nr. 4 for alle skøyeløpere, kan vi skrive
% følgende:
tider = rundetider(:, 4); % Henter ut verdiene i alle rader, i kolonne
                          % 4.

% Matriseverdier kan også settes ved å la et indekseringsuttrykk
% være på venstre side av en tilordning. Eksempeloppgaver:
% - Sett løper nr. to sin første rundetid til 23. 
rundetider(2, 1) = 23; 
% - Sett alle de siste rundetidene til 30.

rundetider(:, end) = 30;
% - Sett rundetid 1 og 3 for løper 2 og 3 til 29.
rundetider(2:3, [1, 3]) = 29;

%% Matriseoperasjoner

% Matlab lar oss som kjent regne:
x = 1 + 2;
% Matlab lar oss like gjerne også regne med matriser. De samme
% operatorene kan brukes med matriser som med vanlige tall. Man kan
% addere:
v1 = [1, 2, 3];
v2 = [3, 2, 1];
v3 = v1 + v2;
disp(v3); % Skriver ut [ 4, 4, 4 ]

% ... subtrahere:
v4 = [9, 7, 5] - v3;
disp(v4); % Skriver ut [ 5, 3, 1 ]

% ... og multiplisere:
u = [ 1, 2, 3 ]; % En rad-vektor
v = [ 3; 2; 1 ]; % En kolonne-vektor
z = u * v;

% Det gjelder imidlertid å være oppmerksom på at operatorene kanskje
% ikke alltid betyr det man tror:
disp(z); % Skriver ut... 10?!

% Matlabs operatorer fungerer nemlig til vanlig slik operatorene gjør
% i matrise-sammenheng. Addisjon og subtraksjon fungerer elementvis,
% men matrisemultiplikasjon er nokså annerledes. Dette gjør at
% divisjon og opphøyd-i, som begge kan sees på som varianter av
% multiplikasjon, heller ikke alltid fungerer elementvis.

% Hvis man ønsker å gjøre en operasjon per element i en matrise,
% kan man i stedet bruke Matlabs operatorer .*, ./ og .^. 
%
% La oss prøve å lage de første fire kvadrattallene, gitt følgende
% matrise:
M = [ 1, 2; 
      3, 4 ];

% Prøver vi å bruke vanlig opphøyd-i, får vi oss kanskje en
% overraskelse:
M2 = M ^ 2;
disp(M2);  % Skriver ut [ 7, 10 ;
           %             15, 22 ];
           % Dette fordi M2 = M^2 = M*M, og matrisemultiplikasjon
           % fungerer ikke elementvis.

% For å generere kvadrattall slik vi ønsker, må vi bruke elementvis
% opphøyd-i:
M2 = M .^ 2; % Opphøy hvert element i annen potens
disp(M2); % Skriver ut [ 1, 4;
          %              9, 16 ];


%% Boolske uttrykk (sannhetsuttrykk)

% I tillegg til å kunne regne ut tallverdier, kan Matlab også regne
% ut logiske verdier: er noe sant eller ikke? Disse uttrykkene
% kalles sannhetsuttrykk, og er helt like aritmetiske uttrykk,
% bortsett fra at det er andre operatorer med lavere presedens. I
% tillegg er svaret en logisk verdi: true eller false. 

a = true; 
b = false;

x = 1 < 2; % Svaret er true
y = 1 >= 2; % Svaret er false
z = 1 + 2 > 2 % Svaret er true: sannhetsoperatorer regnes ut etter
              % aritmetiske.

% Ofte, som i Matlabs utskrift av verdiene true og false, behandles
% veriden true som 1 og verdien false som 0. Man kan for eksempel
% bruke verdiene slik til aritmetiske uttrykk:
c = true + 3; 
disp(c); % Skriver ut 4.

% Det er imidlertid en viktig forskjell på tall og sannhetsverdier som
% handler om matriseindeksering, som vi skal se senere.

% I tillegg til disse relasjonelle operatorene, som sammenlikner to
% verdier, finnes det også operatorer som opererer på
% sannhetsverdier. Man kanskje sjekke om begge verdiene er sanne;
% om minst en av verdiene er sanne; eller om verdien ikke er sann:

tilbud_restaurant = true;
tilbud_kino = false;

blir_superkveld = tilbud_restaurant && tilbud_kino;
blir_fin_kveld = tilbud_restaurant || tilbud_kino;
blir_middag_hjemme = ~tilbud_restaurant;

% Kombinasjoner har lavere presedens enn sammenlikninger, og || har
% lavere presedens enn &&. 
%
% Eksempeloppgave: la oss regne ut om vi får penger til overs gitt
% følgende tilbudspriser på restaurant og kino:
pris_kino = 100;
pris_restaurant = 150;
budsjett = 200;
% Anta at vi vet at vi alltid får råd hvis vi spiser hjemme.

% Uttrykket blir som følger:
blir_penger_til_overs =  blir_middag_hjemme || ...
    blir_superkveld && pris_kino + pris_restaurant < budsjett || ...
    ~tilbud_kino && pris_restaurant < budsjett;

% Uttrykket beskriver tre situasjoner som er de eneste hvor vi har
% råd:
%  1. Det blir middag hjemme.
%  2. Det blir superkveld, altså både middag og kino, og
%  totalprisen er under budsjettet.
%  3. Det er bare tilbud på restaurant, ikke kino, og prisen på det
%  er under budsjettet.

% Siden true og false kan behandles som 1 og 0 i aritmetiske
% uttrykk, kunne vi også skrevet utregningen over som følger:
blir_penger_til_overs =  pris_kino * tilbud_kino + ...
    pris_restaurant * tilbud_restaurant  < budsjett;
% Logikken er som følger: vi regner ut en totalsum basert på pris_kino
% og pris_restaurant. Hvis det ikke er tilbud på kino, ønsker vi å
% ikke legge til prisen for kino. Dette får vi til ved å gange med
% tilbud_kino, som er 0 hvis det ikke er tilbud eller 1 hvis det er
% tilbud. Tilsvarende gjør vi får restaurant-variablene.

% Vi har også logiske uttrykk for matriser.
A = [1, 2, 3] < [3, 2, 1];
disp(A); % Skriver ut [ 1, 0, 0 ]

B = [2, 2, 2] == [1, 3, 2];
disp(B); % Skriver ut [ 0, 0, 1 ]

% Hvis vi skal bruke kombinasjons-operasjonene på matriser og ønsker
% elementvis foperasjon, må vi bruke variantene med bare et enkelt
% tegn (& i stedet for &&, og | i stedet for ||):

disp(A | B); % Skriver ut [ 1, 0, 1 ];
disp(A & B); % Skriver ut [ 0, 0, 0 ];

% Logiske matriser kan også brukes til å indeksere matriser av samme
% størrelse. Dette brukes til å ta med hvert element hvor verdien i
% den logiske matrisen er true. Tilbake til vårt datoeksempel, la
% oss si at vi har følgende vektorer definert:
ukedatoer = [ 7, 8, 9, 10, 11, 12, 13 ];
er_helgedag = [ false, false, false, false, false, true, true ];

% Vi kan hente ut alle helgedatoene fra vektoren 'ukedatoer' ved å
% indeksere den med variabelen 'er_helgedag':
helgedatoer = ukedatoer(er_helgedag);
disp(helgedatoer); % Skriver ut [ 12, 13 ];
% Vi plukker altså med oss hvert element hvor indeks-elementet har
% verdien true.

% Denne typen indeksering brukes gjerne for å hente ut de
% elementene av en matrise som har en viss egenskap. Hvis vi ser på
% våre rundetider igjen, kan vi regne ut verdien på hvorvidt en
% rundetid er under 30 som følger:
under_30 = rundetider < 30;
% Vi kan hente ut disse rundetidene ved å indeksere
% 'rundetider'-matrisen:
raske_tider = rundetider(under_30);
% Vanligvis vil slike uttrykk skrives direkte, uten å lage en ny
% variabel for matrisen med sannhetsverdier:
raske_tider = rundetider(rundetider < 30);

% Eksempeloppgave: la oss hente ut alle rundetidene som er raskere
% enn den raskeste tiden til løper 2.
raskeste_tid_nr2 = min(rundetider(2, :));
tider = rundetider(rundetider < raskeste_tid_nr2);

%Eksempeloppgave: hent tidene hvor løper tre var raskere enn løper
%1.
tider = rundetider(3, rundetider(3,:) < rundetider(1,:));

% Eksempeloppgave: regn ut hvor mange ganger løper tre var raskest.
var_raskest = rundetider(3,:) < rundetider(1,:) & ...
    rundetider(3,:) <    rundetider(2,:);
antall_raskest = sum(var_raskest);
disp(antall_raskest); % Skriver ut 1


%% If-setninger

