-- Variabler
spm 1: Hvilke verdier har variablene?
   
x1 = 3;
x2 = 4;
x3 = x2 - x1;
x2 = x2 - 2;
xs = [x1, x2, x3];
xs2 = x3:x2:x1;

   variasjon: - x3 = -1 eller x3 = 1
              - xs2 = [x3, x2, x1] eller rett svar


   => 1. x1 = 3
         x2 = 2
	 x3 = 1
	 xs = [3, 2, 1]
	 xs2 = [1, 3]
     
      2. x1 = 3
	 x2 = 2
	 x3 = -1
	 xs = [1, 2, -1]
	 xs2 = [-1, 1, 3]

      3. x1 = 3
	 x2 = 2
	 x3 = 1
	 xs = [3, 2, 1]
	 xs2 = [1, 2, 3]

      4. x1 = 3
	 x2 = 2
	 x3 = -1
	 xs = [3, 2, -1]
	 xs2 = [-1, 2, 3]

    

spm 2: Hvilke er gyldige variabeltilordninger?
   x = 3;
   lang_variabel_med_tall_12345 = 5;
   ølpris = 68;
   2x = 3;
   2 * y = 8;
   fem! = 1*2*3*4*5;
Alt: 1;1,2;1,2,3;1,2,3,4;1,2,3,4,5;alle

-- Vektorer
spm 3: Hvilke verdier har xs, ys, og zs? (xs = zeros(5), ys = ones(5), zs = -1:3:7)

	Alternativ: Rett svar, bytt xs og ys, zs er [-1, 2, 5, 7], bytt xs og ys og zs [-1, 2, 5, 7]

spm 4: Hvilke er gyldige måter å konstruere [1, 2, 3, 4, 5] på? ([1, 2, 3, 4,
5], [1 2 3 4 5], 1:5, 1:1:5 - alle er rett, 1 og 3 er rett, 1, 3 og 4 er rett,
1, 2 og 3 er rett, 1 og 4 er rett, 1 og 2 er rett)

spm 5: Gitt vektor xs = 1:5. Hvordan kan du sette første element i xs til 5, og
siste element i xs til 1? Hint: anta at det du ikke har sett før fungerer.

        1. xs(0) = 5;
           xs(4) = 1;

        2. xs(1) = 5;
           xs(5) = 1;

	3. xs(0) = 5; 
	   xs(length(xs)-1) = 1;

	4. xs(1) = 5;
	   xs(length(xs)) = 1;

	5. xs(1) = 5;
	   xs(end) = 1;

	6. xs([1, end]) = [1, 5]


        Alts:
           Alt 1, 3, 5 og 6
           Alt 2, 4, 6 og 6
           Alt 1, 3
           Alt 2, 4

-- Funksjoner
spm 6: Hvordan kalle denne funksjonen? (funksjonen har et annet navn enn filen)
	- filnavn()
	- funksjonsnavn()
	- funksjonen kan ikke kalles

spm 7: Hva skrives til skjerm? Tre funksjoner, tre outputs, seks alternativ.
	 Funksjon 1: Mangler semikolon
	 Funksjon 2: Tilordner ikke returverdivariabelen
	 Funksjon 3: Korrekt

spm 8: Hvilken funksjonsdeklarasjon er mest riktig hvis man vil lage funksjonen
GCD(a, b)? (En bruker tall, en har byttet klamme og parentes, en mangler
function, en har feil navn i forhold til filnavnet, en har for få parametere, en
er korrekt).

spm 9: Hvordan kan man bruke GCD-funksjonen til å regne ut GCD(42, 5)?

spm 10: Test returverdiparameternavn:
  %    Gitt f.dekl function result = f(x)
   
  Hvordan kan du lagre verdien f(17) til variabelen y?
   

-- Til slutt
Hvor mange spørsmål klarte du? (Vær ærlig >:))
Ønsker du en gjennomgang av øving 1?
