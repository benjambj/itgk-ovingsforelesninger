%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 1
x1 = 3;
x2 = x1 + 1;
x1 = x1 - 1;

% Alternativ 1     % Alternativ 2  
x1 = 3             x1 = 2          
x2 = 3             x2 = 4          
                  
% Alternativ 3      % Alternativ 4  
x1 = 2              Koden er ugyldig.
x2 = 3              


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 2
x = 3;
lang_variabel_med_tall_12345 = 5;
ølpris = 68;
2x = 3;
2 * y = 8;
fem! = 1*2*3*4*5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 3
xs = zeros(1, 5);
ys = ones(1, 5);
zs = -1:3:7;

% Alternativ 1             % Alternativ 2         
xs = [ 1, 1, 1, 1, 1 ]     xs = [ 0, 0, 0, 0, 0 ] 
ys = [ 0, 0, 0, 0, 0 ]     ys = [ 1, 1, 1, 1, 1 ] 
zs = [ -1, 2, 5 ]          zs = [ -1, 2, 5 ]      

% Alternativ 3             % Alternativ 4
xs = [ 1, 1, 1, 1, 1 ]     xs = [ 0, 0, 0, 0, 0 ] 
ys = [ 0, 0, 0, 0, 0 ]     ys = [ 1, 1, 1, 1, 1 ] 
zs = [ -1, 2, 5, 7 ]       zs = [ -1, 2, 5, 7 ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 4

% Måte 1             % Måte 2    
[ 1, 2, 3, 4, 5]     [ 1 2 3 4 5]

% Måte 3             % Måte 4
1:5                  1:1:5



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 5
xs = 1:5

% Alternativ 1        % Alternativ 2          % Alternativ 3  
xs(0) = 5;            xs(0) = 5;              xs(1) = 5;      
xs(4) = 1;            xs(length(xs)-1) = 1;   xs(5) = 1;      


% Alternativ 4        % Alternativ 5          % Alternativ 6        
xs(1) = 5;            xs(1) = 5;              xs([1, end]) = [5, 1];
xs(length(xs)) = 1;   xs(end) = 1;    



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 6

% filnavn.m
function result = funksjonsnavn(x)
    result = x * x;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 7

x = funksjon(5)

% Funksjon 1                             % Alternativ 1
function resultat = funksjon(parameter)  resultat =                                                                                 
    resultat = parameter * parameter                                                                                                
end                                          10                                                                                     

                                         x =
% Funksjon 2                                                                                                                        
function resultat = funksjon(parameter)      10                                                                                     
    parameter * parameter;               
end                                                                                                                                 
                                         % Alternativ 2
% Funksjon 3                                    
function resultat = funksjon(parameter)   x =                                                                                       
    resultat = parameter * parameter;                                                                                               
end                                           10
                                          
                                         % Alternativ 3
                                         Error in funksjon (line 2)
                                             parameter + parameter;                                                            
                                                                                                                               
                                         Output argument "resultat" (and maybe others) not assigned during call to             
                                         "C:\Users\benjambj\Documents\MATLAB\øvingsforelesning 2\kahoots\funksjon.m>funksjon".
x = funksjon(5)

% Alternativ 1                                                                       
resultat =                                                                           
                                                                                     
    10                                                                               
                                                                                     
x =                                                                                  
                                                                                     
    10                                                                               
                                                                                     
                                                                                     
% Alternativ 2                                                                       
                                                                                     
 x =                                                                                 
                                                                                     
     10                                                                              
                                                                                     
% Alternativ 3                                                                       
Error in funksjon (line 2)                                                           
    parameter + parameter;                                                           
                                                                                     
Output argument "resultat" (and maybe others) not assigned during call to            
"C:\Users\benjambj\Documents\MATLAB\øvingsforelesning 2\kahoots\funksjon.m>funksjon".



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 8

% Alternativ 1
function d = GCD(a, b)

% Alternativ 2
function GCD(a, b)
        
% Alternativ 3
function d = tullenavn(a, b)

% Alternativ 4
function d = GCD()
        


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 9

% Alternativ 1
function d = Maks(42, 5)

% Alternativ 2
Maks 42 5

% Alternativ 3
Maks(42, 5)

% Alternativ 4
(42, 5)Maks

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spm 10

function result = f(x)
                                       
% Alternativ 1    % Alternativ 2   
y = f(17);        Det går ikke, siden              
                  resultatvariabelen heter 'result'


                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUIZ 2
%
% Spm 1

true && false

% Spm 2

true || false

% Spm 3
~true

% Spm 4
1 < 1

% Spm 5
1 <= 1

% Spm 6

3 > 1 && 3 > 4

% Spm 7

3 > 4 || 3 > 1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% QUIZ 3
%%%%
% Spm 1

a = 100;
if a < 100
    disp('Liten a');
else
    disp('Stor a');
end

% Spm 2
alder = 50;
if alder >= 50
    alder = 49;
    disp('fortsatt ung');
end
disp(alder);

% Spm 3
x = 2;
if x <= 1
    disp(1);
elseif x <= 2
    disp(2);
elseif x <= 3
    disp(3);
else
    disp(4);
end

