function resultat = trizero(A, upper)
    resultat = [];
    lengder = size(A);
    if length(lengder) ~= 2
        return;
    end
    
    rader = lengder(1);
    kolonner = lengder(2);
    if rader ~= kolonner
        return;
    end
    
    n = rader;
    resultat = A;
    for i = 1:n
       for j = i+1:n
           if upper
               resultat(i, j) = 0;
           else
               resultat(j, i) = 0;
           end
       end
    end
end