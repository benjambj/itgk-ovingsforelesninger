function indeks = finn(vektor, element)
    for i = 1:length(vektor)
       if vektor(i) == element
           indeks = i;
           return;
       end
    end
    indeks = 0;
end