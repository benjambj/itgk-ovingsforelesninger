function resultat = begrens(x, min, max)
    resultat = x;
    if x < min
        resultat = min;
    elseif x > max
        resultat = max;
    end
end