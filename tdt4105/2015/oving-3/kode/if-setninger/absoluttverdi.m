function res = absoluttverdi(x)
    if (x < 0)
        res = -x;
    else
        res = x;
end