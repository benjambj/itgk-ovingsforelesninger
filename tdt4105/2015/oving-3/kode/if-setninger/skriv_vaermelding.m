function skriv_vaermelding(temperatur)
    if temperatur <= -50
        disp('Sola har sluknet');
    elseif temperatur <= 10
        disp('Kakaovær');
    elseif temperatur <= 15
        disp('Trondheimssommer');
    else
        disp('Varmt nok');
    end
end
