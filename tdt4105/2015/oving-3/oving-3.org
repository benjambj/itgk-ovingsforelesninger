# -*- org-beamer-outline-frame-title:  Innhold -*-

#+TITLE:     Øvingsforelesning TDT4105 Matlab
#+AUTHOR:    Benjamin A. Bjørnseth
#+EMAIL:     benjambj@idi.ntnu.no
#+DATE:      \today{}
#+DESCRIPTION: Oppgaver med øving 2 (og litt 3) pensum.
#+KEYWORDS: øving 2, øving 3, tdt4105, matlab

#+LANGUAGE:  no
#+latex_header: \usepackage[norsk]{babel}
#+BEAMER_THEME: ntnubokmaal

#+latex_header: \subtitle{Pensum fra øving 2 og 3: if, switch, for, matriser.}

#+STARTUP: beamer

#+LATEX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation,smaller]

#+OPTIONS:   H:2 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:nil mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME:
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

#+latex_header: \renewcommand\maketitle{\ntnutitlepage}
#+latex_header: \renewcommand\alert{\textbf}

#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsection]\end{frame}}
#+latex_header: \AtBeginSubsection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsubsection]\end{frame}}

#+latex_header: \usepackage{minted}

* Kladd for ny versjon                                               :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:

** Innhold i øving 3:
:LOGBOOK:
CLOCK: [2015-09-14 ma. 15:36]--[2015-09-14 ma. 15:59] =>  0:23
:END:
- [-] For-løkke:
  - [-] Telling 
  - [-] Multiplikasjonsreduksjon med element
  - [-] Oppdatering av variabel
  - [-] Potensielt doble for-løkker

- [ ] If/elseif:
  - [ ] Manglende returverdi
    
    Obskur case... men kanskje den skal dekkes av et eksempel som ikke
    har med noen else? Jupp, nå dekket med funksjonen =begrens()= (aka
    =clamp()=)
    
    

- [-] Switch:
  - [-] =otherwise= 
  - [-] Flere ting i case
    - Det kan dekkes gjennom romertalloppgaven: I, II og III kan håndteres relativt likt! 

- [-] Betingelser: 
  - [X] ==
  - [ ] ~=
  - [X] &&
    (Dekkes av første 

  - [ ] ||

- [-] Funksjoner:
  - [ ] =mod=   
    Dropper =mod=, for den tok vi forrige time.
  - [X] =reshape= 

- [ ] =fprintf=:
  - [ ] =%d=
  - [ ] =\n=

- [-] Matriseoperasjoner:
  - [ ] Potensielt matrisemultiplikasjon
  - [ ] Multiplikasjon m/ skalar
  - [ ] Addisjon
  - [X] Indeksering

  (Tror vi lar det være med indeksering... eksemplene med
  matrisemultiplikasjon (som total poengsum gitt vektet
  poengfordeling) er litt kunstige!)

- [ ] While-løkke:
  - [ ] Kanskje..  
    Dropper while-løkke: Får ikke tid, og while introduseres senere
    enn for.
  

- [-] Cellelister?!
  - [ ] Oppslag (egentlig ikke.. det brukes bare av scriptet,
    studentene trenger bare matriseoperasjoner).
  - [X] Forsåvidt også i switch.. kanskje verdt å nevne.

** Mulige eksempeloppgaver

*** Enkle if-oppgaver (rest fra forrige forelesning)                   :if:
- Definitivt kjekt.

*** Romertall                                         :error:strcmp:switch:

*** For-løkke-oppgaver                                                :for:
- Vektorlengde-oppgaven kan også løses med matriseoperasjoner..

*** Eksamensoppgave: trizero                                       :if:for:
- .. er bare dobbel for-løkke.
- .. men det er kanskje OK?

*** For-løkke-romertall                               :error:if:switch:for:

*** +Elementvis operasjon+
- Skriv ut hvilken elementvis operasjon som tilsvarer en annen...
- .. den trengte horzcat... blæ!


***  Finn en oppgave som involverer matriseoperasjoner, fprintf og switch med flere alternativ per case..

*** Kø-oppgave                                        :indeksering:reshape:
- Gitt en matrise $M_1$ med dimensjoner $m \times n$. Lag en ny
  matrise $M_2$ med dimensjoner $m \times n-1$, hvor de $n$ minste
  elementene er fjernet. Et fjernet element fylles av elementet på
  raden under.
- Plan:
  - Gjør kø-matrisen til en kø-vektor, hvor første element er først i køen
  - Finn de $n$ personene i køen med dårligst tid.
  - Lag en matrise med alle de andre personene.
- Triks:
  - Hente ut antall rader og kolonner for matrise: =[m, n] = size(matrise)=
  - Finne posisjonene til en liste sortert etter stigende verdi: =[~, ordning] = sort(liste)=
  - Lag en $m \times n$ matrise fra en liste: =reshape(L, m, n)=

** Forslag til plan: 

*** Repetisjonskahoot
- 2-4 spørsmål om funksjoner og matriseindeksering?

*** If-oppgaver
- To enkle, hvor i hvert fall en er avhengig av rekkefølgen man tester.
  - Kan ta absoluttverdi og værmelding, kanskje.

*** Switch-oppgave
- Romertall.

*** For-løkke-oppgaver
- Får dårlig tid hvis jeg tar alle sammen...
- .. men så er jo for-løkker litt vanskelig, da...
- Vektorlengde

* If-setninger
** Repetisjon
   - For de som ikke jobbet med Matlab i helgen

***                                                                 :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.333
    :END:

#+begin_src matlab
if <betingelse>
    <kode>
end
#+end_src

***                                                                 :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.333
    :END:

#+begin_src matlab
if <betingelse>
    <kode1>
else
    <kode2>
end
#+end_src


***                                                                 :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.333
    :END:

#+begin_src matlab
if <betingelse1>
    <kode1>
elseif <betingelse2>
    <kode2>
...
elseif <betingelseN>
   <kodeN>
else
   <kodeN+1>
end
#+end_src



** Oppgaver

    - <1|only@1> Skriv ut værmelding basert på temperatur.
       | Temperatur     | Melding             |
       |----------------+---------------------|
       | -\infty -- -50 | Sola har sluknet    |
       | -50 -- +10     | Kakaovær            |
       | +10 -- +15     | Trondheimssommer    |
       | +15 -- \infty  | Varmt nok           |


    - <2|only@2> Implementer følgende mattefunksjon

      \begin{equation*}
      absoluttverdi(x) = 
      \begin{cases}
      x & \text{if } x >= 0 \\
      -x & \text{if } x < 0
      \end{cases}
      \end{equation*}

   - <3|only@3> Lag funksjonen =begrens(x, min, max)=, som returnerer
     verdien nærmest $x$ i intervallet $\lbrack min, max \rbrack$. Eksempler:
     - =begrens(5, 1, 10)= $\rightarrow$ =5=
     - =begrens(-5, 1, 10)= $\rightarrow$ =1=
     - =begrens(15, 1, 10)= $\rightarrow$ =10=

* Switch
** Format
  - Spesialvariant av /if/ tiltenkt situasjoner med mange
    likhetssammenlikninger.

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:

#+begin_src matlab
switch verdi
    case verdi1
        <kode1>
    case verdi2
        <kode2>
    ...
    case verdiN
        <kodeN>
    otherwise
        <kodeN+1>
end

#+end_src


***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:

#+begin_src matlab
if verdi == verdi1
    <kode1>
elseif verdi == verdi2
    <kode2>
...
elseif verdi == verdiN
    <kodeN>
else
    <kodeN+1>
end

#+end_src

** Eksempel: romertall
#+ATTR_LATEX: :align cc|cc
   | 0 | 'nulla' |  6 | 'VI'   |
   | 1 | 'I'     |  7 | 'VII'  |
   | 2 | 'II'    |  8 | 'VIII' |
   | 3 | 'III'   |  9 | 'IX'   |
   | 4 | 'IV'    | 10 | 'X'    |
   | 5 | 'V'     |    |        |

  1. Lag en funksjon som tar inn et tall mellom 0 og 10 og returnerer rett
     romertall.
     - Eks: romertall(5) gir 'V'
    
* For-løkker

** Format
   - Kjører kode flere ganger.

#+begin_src matlab
for <variabel> = <vektor>
    <kode>
end
#+end_src

\pause

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.33
    :END:
#+begin_src matlab
for i = 1:10
    disp(i);
end
#+end_src

\pause

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.33
    :END:
#+begin_src matlab
v = [1, 5, 9]
for element = v
    disp(element);
end
#+end_src

\pause

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.33
    :END:
#+begin_src matlab
A = magic(3);
for kolonne = A
    disp(kolonne);
end
#+end_src

** Eksempel: vektoroperasjoner
#+ATTR_BEAMER: :overlay +-
   1. Lag en funksjon =summer(vektor)=, som returnerer summen av alle elementene i
      en vektor.
      - Innebygd variant: =sum(vektor)=
   2. Lag en funksjon =finn(vektor, element)=, som returnerer første plassen i
      vektoren med elementet. Hvis det ikke finnes, returner 0.
      - Innebygd variant: ~find(vektor == element)~
      - Eller =[~, indeks] = ismember(element, vektor)=
   3. Lag en funksjon =vektorlengde(vektor)= som regner ut euklidsk lengde av
      vektoren. Formel: $\lvert v \rvert = \sqrt{v_1^2 + v_2^2 + ... + v_n^2}$
      - Innebygd variant: =norm(vektor)=


* Diverse

** Eksempel: flyplasskøer
- Tenkt situasjon: flyplasskø (sikkerhetskontroll)
  - Folk går i rader
  - Hver person i køen har et visst antall minutter til flyet går

** Eksempel: flyplasskøer
[[file:figures/sikkerhetskontroll-3-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/sikkerhetskontroll-2-left.pdf]]

** Eksempel: flyplasskøer 
- Oppgave: lag et program som henter de personene med dårligst tid ut
  av køen, slik at kørekken får en kolonne mindre.

** Eksempel: flyplasskøer
#+ATTR_LATEX: :width 0.7\textwidth
[[file:figures/sikkerhetskontroll-2-left.pdf]]
** Eksempel: flyplasskøer
#+ATTR_LATEX: :width 0.7\textwidth
[[file:figures/sikkerhetskontroll-1-left.pdf]]
** Eksempel: flyplasskøer
#+ATTR_LATEX: :width 0.7\textwidth
[[file:figures/sikkerhetskontroll.pdf]]

** Først et par Matlabtips
#+ATTR_BEAMER: :overlay +-
- Hente ut antall rader og kolonner for matrise: 
  - =[m, n] = size(matrise)=
- Finne posisjonene til en liste sortert etter stigende verdi: 
  - =[~, ordning] = sort(liste)=
- Lag en $m \times n$ matrise fra en vektor:
  - =reshape(vektor, m, n)=

** Køen som matrise
[[file:figures/ko-som-matrise.pdf]]
** Køen som matrise
[[file:figures/ko-som-matrise-1-left.pdf]]

** Løsningsplan:
1. Brett ut køen ved å gjøre kø-matrisen til en kø-vektor.
2. Finn de $n$ personene i køen med dårligst tid.
3. Lag en matrise med alle de andre personene.


** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko-5-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko-4-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko-3-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko-2-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko-1-left2.pdf]]

** Eksempel: flyplasskøer
Matrise som vektor: =M(:)=

[[file:figures/matriseko-som-vektorko-1-left2.pdf]]



** Eksempel: flyplasskøer
[[file:figures/matriseko-som-vektorko.pdf]]

** Eksempel: flyplasskøer
[[file:figures/finn-daarligst-tid.pdf]]
** Eksempel: flyplasskøer
Finn rekkefølgen på liste: =[~, ordning] = sort(liste)=
[[file:figures/finn-daarligst-tid.pdf]]

** Eksempel: flyplasskøer
[[file:figures/fjern-daarligst-tid-1-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/fjern-daarligst-tid.pdf]]
** Eksempel: flyplasskøer
Hent ut element fra posisjoner i liste: =liste(posisjoner)=

(Hentes ut i rekkefølgen gitt i =posisjoner=)
[[file:figures/fjern-daarligst-tid.pdf]]


** Eksempel: flyplasskøer
[[file:figures/lag-ny-komatrise-4-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/lag-ny-komatrise-3-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/lag-ny-komatrise-2-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/lag-ny-komatrise-1-left.pdf]]
** Eksempel: flyplasskøer
[[file:figures/lag-ny-komatrise.pdf]]

** Eksempel: flyplasskøer
Lag $m \times n$ matrise fra vektor: =reshape(vektor, m, n)=
[[file:figures/lag-ny-komatrise.pdf]]

** Eksempel: romertall
   1. Lag en funksjon som tar inn et romertall, og returnerer tallverdien.
      - =fra_romertall('MCMXCIX')= -> 1999
   
** Eksempel: eksamensoppgave
   [[file:figures/eksamen-h2008.png]]

