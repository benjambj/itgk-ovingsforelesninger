function sekvens = lengste_felles_delstreng(genom1, genom2)
    lengder = zeros(length(genom1) + 1, length(genom2) + 1);
    maks_lengde = 0;
    posi = 0;
    for i = 2:length(genom1)+1
        for j = 2:length(genom2)+1
            if genom1(i-1) ~= genom2(j-1)
                lengder(i, j) = 0;
            else
                lengder(i, j) = lengder(i-1, j-1) + 1;
                if lengder(i, j) > maks_lengde
                    maks_lengde = lengder(i, j);
                    posi = i-1;
                end
            end
        end
    end
    sekvens = genom1(posi-maks_lengde+1:posi);
end