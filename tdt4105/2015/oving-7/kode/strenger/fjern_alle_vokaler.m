function resultat = fjern_alle_vokaler(streng)
    resultat = streng(er_vokal(streng));
end