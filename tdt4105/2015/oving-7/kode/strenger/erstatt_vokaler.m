function res = erstatt_vokaler(streng, vokalliste)
    ord = strsplit(streng);
    for i = 1:length(ord)
        ord{i}(er_vokal(ord{i})) = vokalliste(mod(i-1, length(vokalliste))+ 1);
    end
    res = strjoin(ord, ' ');
end