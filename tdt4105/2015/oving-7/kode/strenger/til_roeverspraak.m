function res = til_roeverspraak(streng)
    persistent impl;
    if isempty(impl)
        symbols = ['a':'z' 'A':'Z' '0':'9'];
        randsymbols = randi(numel(symbols), 1, 1e7);
        teststreng = symbols(randsymbols);
        impl = time_implementations({@til_roeverspraak_loop, ...
                                     @til_roeverspraak_vector, ...
                                     @til_roeverspraak_dupliser}, ...
                                      teststreng);
    end
    res = impl(streng);
end

function res = til_roeverspraak_loop(streng)
    konsonanter = er_konsonant(streng);
    res = blanks(length(streng) + 2*length(konsonanter));
    resi = 1;
    for i = 1:length(streng)
        if konsonanter(i)
            res(resi) = streng(i);
            res(resi+1) = 'o';
            res(resi+2) = streng(i);
            resi = resi + 3;
        else
            res(resi) = streng(i);
            resi = resi + 1;
        end
    end
end

function res = til_roeverspraak_vector(streng)
    er_kons = er_konsonant(streng);
    res = blanks(length(streng) + 2*length(er_kons));
    konsts_foer = [0, cumsum(er_kons(1:end-1))];
    nye_plasser = (1:length(streng)) + 2*konsts_foer;
    res(nye_plasser) = streng;
    res(nye_plasser(er_kons) + 1) = 'o';
    res(nye_plasser(er_kons) + 2) = streng(er_kons);
end

function res = til_roeverspraak_dupliser(streng)
    utvidet = repmat(streng, 3, 1);
    utvidet(2,:) = 'o';
    er_kons = er_konsonant(streng);
    res = blanks(length(streng) + 2*length(er_kons));    
    for i = 1:length(streng)
        if er_kons(i)
            res(i:i+2) = utvidet(:, i);
        else
            res(i) = streng(i);
        end
    end 
end
