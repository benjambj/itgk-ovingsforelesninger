function res = fra_roeverspraak(streng)
    persistent impl;
    if isempty(impl)
        symbols = ['a':'z' 'A':'Z' '0':'9'];
        randsymbols = randi(numel(symbols), 1, 1e7);
        teststreng = symbols(randsymbols);
        impl = time_implementations({@fra_roeverspraak_littloop, ...
                                     @fra_roeverspraak_ikkeloop}, ...
                                      teststreng);
    end
    res = impl(streng);
end



function res = fra_roeverspraak_littloop(streng)
    er_kons = er_konsonant(streng);
    antall = cumsum(er_kons);
    ugyldige = er_kons;
    for i = 1:length(ugyldige)
        if mod(antall(i), 2) == 1 && er_kons(i)
            ugyldige(i) = false;
            ugyldige(i+1) = true;
        end
    end
    
    res = streng(~ugyldige);
end

function res = fra_roeverspraak_ikkeloop(streng)
    er_kons = er_konsonant(streng);
    antall = cumsum(er_kons);
    siste_kons = ~mod(antall, 2) & er_kons;
    foer_siste_kons = [siste_kons(2:end), false];
    ugyldige = siste_kons | foer_siste_kons;
    res = streng(~ugyldige);
end