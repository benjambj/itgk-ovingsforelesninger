%% Kode fra �vingsforelesning 7.
function[E, D, m] = lag_noekler()
    m = 257; % Alfabet = [0, m-1], m b�r settes til primtall.
    size = 5;
    E = randi(m-1, size);
    d = round(mod(det(E), m));
    while gcd(d, m) ~= 1  % Trenger denne egenskapen for � finne multiplikativ invers      
        E = randi(m-1, size);
    end
    dinv = mod(multiplicative_inverse(d, m), m); % Exists since gcd(d, m) == 1
    C = mod(adjugate(E), m);
    D = mod(dinv * C, m);    
end

function dinv = multiplicative_inverse(d, m)
    [dinv, ~] = extended_euclid(d, m);
end

function [x, y] = extended_euclid(a, b)
    x = 1;
    xn = 0;
    y = 0;
    yn = 1;
    while b ~= 0
        q = floor(a / b);
        t = a;
        a = b;
        b = t - q*b;
        t = x;
        x = xn;
        xn = t - q*xn;
        t = y;
        y = yn;
        yn = t - q*yn;
    end
end

function C = adjugate(A)
    C = zeros(length(A));
    for i = 1:size(A, 1)
        for j = 1:size(A, 2)
            M = round(det(A([1:i-1 i+1:end], [1:j-1 j+1:end]))); % First minor
            C(j,i) = (1 - 2*mod(i+j,2))*M;                % Negate every other, transpose
        end
    end
end