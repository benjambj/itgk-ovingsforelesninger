function skriv_lagerstatus(varer)
    for i = 1:length(varer)
        vare = varer(i);
        % Husk at man kan spesialisere fprintf-format ved hjelp av ekstra
        % bokstaver mellom %-tegnet og format-bokstaven. For eksempel: ved
        % � skrive et tall mellom % og s, sier man at man vil at strengen
        % skal bruke minst s� mange bokstaver ved utskrift. Hvis strengen
        % ikke er lang nok, vil det skrives ut ekstra mellomrom. Et negativt
        % tall gj�r strengen venstre-justert: ekstra bokstaver legges til
        % til slutt.
        fprintf('%-30s %10d @ %10.2f kr\n', ...
            vare.navn, vare.antall, vare.pris);
    end
end