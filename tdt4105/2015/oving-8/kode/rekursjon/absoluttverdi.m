%% Ikke gj�r dette
% Dette er en dum implementasjon av absoluttverdi som bare fungerer for
% heltall og som krasjer for store tall. Det er kun ment som en �velse i
% rekursjon.
function res = absoluttverdi(x)
    if x == 0
        res = 0;
    elseif x < 0
        res = 1 + absoluttverdi(x + 1);
    else
        res = 1 + absoluttverdi(x - 1);
    end
end