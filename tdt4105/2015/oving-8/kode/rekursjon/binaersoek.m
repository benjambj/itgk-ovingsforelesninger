function resultat = binaersoek(liste, verdi)
    if isempty(liste)
        resultat = false;
        return;
    end
    
    midt = ceil(length(liste) / 2);
    if liste(midt) < verdi
       resultat = binaersoek(liste(midt+1:end), verdi);
    elseif liste(midt) > verdi
        resultat = binaersoek(liste(1:midt-1), verdi);
    else
        resultat = true;
    end
end