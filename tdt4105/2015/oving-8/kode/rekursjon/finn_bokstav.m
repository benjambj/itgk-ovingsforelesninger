% Dette er heller ikke videre effektivt: bedre å bruke en for-løkke
% eller ~isempty(find(streng == bokstav))
function resultat = finn_bokstav(streng, bokstav)
    if isempty(streng)
        resultat = false;
    elseif streng(1) == bokstav
        resultat = true;
    else
        resultat = finn_bokstav(streng(2:end), bokstav);
    end
end