function skriv_kvittering(varekjoep)
    for i = 1:length(varekjoep)
        dots = repmat('.', 1, 50);
        fprintf('%s%.*s%d * %d = %d\n', varekjoep(i).navn, 50 - ...
                length(varekjoep(i).navn), dots, varekjoep(i).antall, ...
                varekjoep(i).pris, varekjoep(i).antall * varekjoep(i).pris);
    end
end