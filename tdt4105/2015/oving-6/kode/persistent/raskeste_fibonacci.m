function fibs = raskeste_fibonacci(n)
    persistent impl;
    if isempty(impl)
        tic;
        fibonacci_med_preallokering(1e6);
        t1 = toc;
        fprintf('Tid fibonacci med preallokering: %.2f s\n', t1);
        tic;
        fibonacci_uten_preallokering(1e6);
        t2 = toc;
        fprintf('Tid fibonacci uten preallokering: %.2f s\n', t2);
        if t1 > t2
            fprintf('Bruker fibonacci uten preallokering\n');
            impl = @fibonacci_uten_preallokering;
        else
            fprintf('Bruker fibonacci med preallokering\n');
            impl = @fibonacci_med_preallokering;
        end
    end
    fibs = impl(n);
end

function fib = fibonacci_med_preallokering(n)
    fib = ones(1, n);
    for i = 3:n
        fib(i) = fib(i-1) + fib(i-2);
    end
end

function fib = fibonacci_uten_preallokering(n)
    fib = [1];
    if n == 1
        return;
    end
    fib(2) = 1;
    for i = 3:n
        fib(i) = fib(i-1) + fib(i-2);
    end
end