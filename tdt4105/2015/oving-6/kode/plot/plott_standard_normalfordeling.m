function plott_standard_normalfordeling()
    plot(-10:0.1:10, f(-10:0.1:10));
end

function resultat = f(x)
    resultat = 1 / sqrt(2*pi) * exp(-x.^2/2);
end