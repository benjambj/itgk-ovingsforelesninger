assert(h(1, 1) == 1);
assert(h(1, 2) == 0.5);
assert(h(2, 1) == 2);
assert(h(2, 2) == 2);

assert(all(h(1:100, 1:100) == arrayfun(@h, 1:100, 1:100)));

% Use h() to estimate e^x!
tolerance = 1e-6;
assert(abs(sum(h(0, 0:100)) - exp(0)) < tolerance);
assert(abs(sum(h(1, 0:100)) - exp(1)) < tolerance);
assert(abs(sum(h(17, 0:100)) - exp(17)) < tolerance);

disp('All tests of h() passed');