function test_fibonaccier()
    disp('Fibonacci med preallokering');
    tic
    fibs1 = fibonacci_med_preallokering(100000);
    toc
    disp('Fibonacci uten preallokering');
    tic
    fibs2 = fibonacci_uten_preallokering(100000);
    toc
    assert(all(fibs1 == fibs2));
end

function fib = fibonacci_med_preallokering(n)
    fib = ones(1, n);
    for i = 3:n
        fib(i) = fib(i-1) + fib(i-2);
    end
end

function fib = fibonacci_uten_preallokering(n)
    fib = [1];
    if n == 1
        return;
    end
    fib(2) = 1;
    for i = 3:n
        fib(i) = fib(i-1) + fib(i-2);
    end
end