function test_integral()
    areal = integral(@f, 0, 100);
    fprintf('Resultatet av � integrere f fra 0 til 100 er %d\n', areal);
    areal = integral(@(x) x.^2, 0, 100);
    fprintf('Resultatet av � integrere x.^2 fra 0 til 100 er %d\n', areal);
    
    
    areal = integral(@(x) (disp('kalt'); ones(1, length(x))), 0, 100);
    fprintf('Resultatet av � integrere konstant-funksjonen 1 fra 0 til 100 er %d\n', areal);
end

function resultat = f(x)
    resultat = x .^ 2;
end