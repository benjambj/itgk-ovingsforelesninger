function resultat = h(x, i)
    resultat = x .^ i ./ factorial(i);
end