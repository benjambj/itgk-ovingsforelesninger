function rot = kvadratrot(n)
    rot = finn_nullpunkt(@(x) x^2 - n, @(x) 2*x);
end