function x = kvadratrot_steg(n)
    kapasitet = 20;
    x = zeros(1, kapasitet);
    i = 1;
    x(i) = n / 2;
    while ikke_noeyaktig_nok(x(i), n, 1e-6)
        i = i+1;
        if i > kapasitet
            % Ikke mer plass: doble kapasiteten.
            x(i:2*kapasitet) = 0;
            kapasitet = kapasitet * 2;
        end
        x(i) = x(i-1) - (x(i-1)^2 - n)/(2 * x(i-1));
    end    
    x = x(1:i); % Forkort vektor til bare � ha elementer med verdier.
end

function resultat = ikke_noeyaktig_nok(old_x, n, toleranse)
    resultat = abs(old_x^2 - n) >= toleranse;
end
    