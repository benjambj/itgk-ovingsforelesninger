% Oppgave 1
% Lag en vektor med de fem f�rste positive oddetallene. Lagre den i
% variablen v1

% L�sning 1: skriv alle elementene selv
v1 = [ 1, 3, 5, 7, 9 ]
% L�sning 2: bruk kolon-operatoren til � generere listen. Antar at vi vet
% at siste element skal v�re 9
v1 = 1:2:9
% L�sning 3: bruk kolon-operatoren til � generere listen uten � anta at vi
% vet hva siste element skal v�re. I stedet regner vi ut dens verdi vha
% uttrykket (start + steglengde * (antall_element - 1)). I v�rt tilfelle
% blir dette (1    +      2     * (       5       - 1))
v1 = 1:2:(1+2*(5-1))

% Oppgave 2
% Lag en vektor med heltallene fra 10 ned til 0.

% Start p� 10, steglengde -1, slutt p� 0
v2 = 10:-1:0

% Oppgave 3
% Lag en vektor med tallene [0.1, 0.2, ..., 1.0]

% Start p� 0.1, steglengde 0.1, slutt p� 1
v3 = 0.1:0.1:1

% Oppgave 4
% Summer de siste elementverdiene i vektorene v1, v2 og v3

% L�sning 1
v1(5) + v2(11) + v3(10)

% L�sning 2
v1(end) + v2(end) + v3(end)

% L�sning 3
v1(length(v1)) + v2(length(v2)) + v3(length(v3))
