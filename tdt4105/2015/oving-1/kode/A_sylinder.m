function A = A_sylinder(r, h)
    A = 2 * pi * r * h + 2 * pi * r^2;
    % Kommentarer:
    %  - Vi st�tter ikke vektor-verdier i parametrene r og h
    %  - Vi skriver semikolon til slutt i koden i funksjonen, slik at vi
    %    ikke f�r un�dvendig mye utskrift.
    %  - Resultatet av funksjonen lagres i returverdivariabelen 'A', slik
    %    at det er tilgjengelig for de som kaller (bruker) funksjonen.
end