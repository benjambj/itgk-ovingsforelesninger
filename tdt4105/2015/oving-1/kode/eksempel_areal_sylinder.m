% Regn ut areal for sylindrene med
%  - radius 3, h�yde 7
%  - radius 1, h�yde 8
%  - radius 9, h�yde 3

radiuser = [3, 1, 9]
hoyde = [7, 8, 3]
areal = 2 * pi * radiuser .* hoyde + 2 * pi * radiuser .^ 2

% Kommentar: vi kan bruke vanlig multiplikasjonsoperator der vi ganger et
% vanlig tall (en skalar) med en liste - det tilsvarer elementvis
% multiplikasjon. Der vi vil gange hvert element av (2*pi*radiuser) med
% hvert element i hoyde, derimot, m� vi bruke en elementvis multiplikasjon.
% For "opph�yd-i"-operasjonen, m� vi vite at "opph�yd-i" er en
% spesielt definert matriseoperasjon som kun fungerer p� kvadratiske
% matriser. Siden vi �nsker � opph�ye hvert element av radiuser med 2,
% bruker vi en elementvis operator.