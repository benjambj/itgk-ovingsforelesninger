# -*- org-beamer-outline-frame-title:  Innhold -*-

#+TITLE:     Øvingsforelesning i Matlab (TDT4105)
#+AUTHOR:    Benjamin A. Bjørnseth
#+EMAIL:     benjambj@idi.ntnu.no
#+DATE:      \today{}
#+DESCRIPTION: Øving 1
#+KEYWORDS: tralala

#+LANGUAGE:  no
#+latex_header: \usepackage[norsk]{babel}
#+BEAMER_THEME: ntnubokmaal

#+STARTUP: beamer

#+LATEX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation,smaller]

#+OPTIONS:   H:2 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:nil mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME:
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsection]\end{frame}}
#+latex_header: \AtBeginSubsection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsubsection]\end{frame}}

#+latex_header: \renewcommand\maketitle{\ntnutitlepage}
#+latex_header: \renewcommand\alert{\textbf}
#+latex_header: \subtitle{Øving 1. Frist: 11.09. Tema: matematiske uttrykk, variabler, vektorer, funksjoner.}

#+latex_header: \usepackage{minted}
#+latex_header: \usepackage{gensymb}

* TIPS TIL NESTE ÅR                                                  :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:

** Tidlig scripts
- Kanskje introdusere script tidlig som en måte å samle kode på?
- Parametriser scriptene ved å 'anta' at det finnes variabler med et
  visst navn, og 'anta' at scriptene lagrer resultatene i variabler
  med gitte navn.
- Deretter vis hvordan funksjoner gjør denne overføringen av verdier
  bedre.

- Tenkte fordeler:
  - Det gjør studentene vant med tanken på å samle kode for å gjøre noe.
  - Trenger ingen nye konsept - bare en fil med Matlab-kode slik som
    man skriver det i kommandovinduet.
  - Det motiverer scripts: kjøre en rekke kommandoer om igjen.
  - Det gjør forskjellen på funksjoner og scripts tydeligere.
  - Det motiverer også funksjoner: gjør verdioverføringen enklere.
    - I stedet for 'radius = 3; hoyde = 4; regn_ut_volum; disp(resultat)'
      har man 'disp(regn_ut_volum(3, 4))'

** Mindre snakk
- Biten "om øvingsforelesninger" er for lang. Bør kutte mye, som feks:
  - Hvordan prøve å løse problem i Matlab. Om dette skal med må det
    demonstreres.... men det er også vanskelig.

* Praktisk informasjon

** Kahoot
   - Oppvarming!

** Øvingsopplegget
   - Mål: relevant trening i fagets pensum før eksamen
     - Litt teori
     - Matlab
   - Krever registrering på http://itgk.idi.ntnu.no

** Øvingsopplegget
   - Tilgjengelige datamaskiner med Matlab på datasal
   - Kan også løses på egen datamaskin
     - Spør orakeltjenesten om installasjon.
     - Potensielt via remote desktop til datasalmaskiner: ts-stud11.idipc.idi.ntnu.no.
       
   - Øvingene må godkjennes av studass på datasal før frist
   - Kan *ikke* godkjennes elektronisk
     - Besvarelsen må forsvares på datasal
   - Studass vil gi dere veiledning
   - Piazza kan også brukes til spørsmål

** Øvingsopplegget

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
   1. Registrer deg på itgk.idi.ntnu.no
   2. Du får en tilfeldig valgt studass
   3. Studass sender deg epost om hvor og når du skal møte for hjelp og
      godkjenning.
   4. Møt opp på studasstimene og få godkjent før fristen
      - Nytt av året: kan også levere digitalt frem til faktisk frist
        på fredag, og forsvare besvarelsen for studass innen onsdag
        uka etter.

***                                                                   :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    [[file:lokasjon-datasal.png]]

** Nettsider
   - itgk.idi.ntnu.no :: Øvinger, foiler, beskjeder, pensum, annen info.
   - piazza.com :: Spørsmål og svar.

Fordeler med Piazza:
  - Rask responstid.
  - Kan jobbe med andre ting mens man venter på svar.
  - Læringsutbytte i å hjelpe andre.
  - Muligens hjelp å få ved kvelds- og helgejobbing.

* Om øvingsforelesninger

** Tidspunkt
   - Øvingsforeleseninger i Matlab 
     - Tirsdag 10.15 - 12.00 (parallell M1)
     - Onsdag 12.15 - 14.00 (parallell M3)
     - Torsdag 12.15 - 14.00 (parallell M2)
   - Hold deg til din parallell
     - Men det finnes altså backup

** Målgruppe
   - De som ikke synes det er kjempelett.
     - Vi prøver å unngå dypdykk utenfor pensum.
   - Vanskelige spørsmål mottas med takk
     - (men det er mulig de ikke blir besvart før etter timen)

** Innhold i øvingsforelesningene
   - (Gå gjennom løsning på forrige øving)
     - Hvis relevant - etter hvert også om ønskelig
   - Gå gjennom oppgaver som bruker konsept som trengs for å løse neste øving.
   - Ikke fokus på teori
   - Fullt fokus på programmering
     - Ta med egen PC!

** Filosofi bak innhold
   - Teori kan man lese i boka 
   - Programmering må man øve på
     - Men øvelse gjør mester!
     - Alle kan lære dette
   - Tiden er knapp, og interessen muligens variabel
     - Vil bruke timene til å gi to timer ekstra praksis i uka.
   - <2-> Med mindre teori blir etterspurt
     - Kom gjerne med innspill (finnes tilbakemeldingsskjema på itgk.idi.ntnu.no).
     - Timene er til for deres hjelp
***                                                                  :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:
     - Øvelse gjør mester
     - Alle kan dette
     - Men tiden er knapp for mange
       - Og mange er ikke like interessert
     - Vil bruke timene til å gi to timer ekstra praksis i uka.

   - (med mindre teori blir veldig eterspurt)
   - (kom gjerne med innspill direkte, eller via referansegruppe -
     timene er her for at dere skal lære mest mulig, og de mest
     høylytte har en tendens til å få det som de vil!)

** Øvelse!
   - Kan ikke sies for ofte, jo mer du prøver jo mer lærer du.
   - Det som er fint med Matlab, er at det er veldig lett å prøve!
     - Det verste som kan skje, er at programmet ikke fungerer.

** Problemressurser
   - Problemressurs #1: Øvinger
     - Ikke fokuser på minimum, vær så snill :)
   - Problemressurs #2: Oppgaver i læreboka
\hrulefill
   - Problemressurs #3: https://projecteuler.net/
   - Problemressurs #4: http://coderbyte.com
   - Problemressurs #5: http://www.reddit.com/r/dailyprogrammer

** Usikker på Matlab?
   1. Skriv noe (hva som helst) i kommandovinduet
      - Gjerne med utgangspunkt i bok, foiler og øvingstekst
      - Ha en forventning til hva som skal skje
   2. Prøv å forstå feilmeldingen
      - Bruk =help=
      - Spør på piazza
      - Google feilmeldingen
      - Spør studass
   3. Prøv å fikse feilen fra feilmeldingen
      - Bruk =help=
      - Spør på piazza
      - Spør studass
   4. Tada!

***                                                                  :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:
   - Usikker på noe?
     - Skriv noe (hva som helst) i modulen
     - Prøv å forstå feilmeldingen (det gir mening til slutt)
       - Spør på piazza om feilmeldingen (andre lurer på det samme)
       - Google feilmeldingen
     - Prøv å fiks feilen fra feilmeldingen
       - Dra til studass og spør
         - De er der 6 timer i uken
           - IKKE BARE FOR Å GODKJENNE
         - Planlegg gjerne å jobbe med øvingen på datasalen når studassen din er der
           - Da kan du få personlig oppfølging og hjelp umiddelbart
         (:Digresjon: om studassbruk
             - Det er travelt de siste timene hvor alle vil ha godkjenning
             - Optimal bruk av studass krever at man begynner på øvingen før siste sjanse.
             - Dette er aller mest mulig om man havner der i starten
             - Havnet på etterskudd?
               - Bruk studass til å komme ajour
               - I tillegg gir auditorieøvingsuker et lite pusterom mtp innleveringsfrister)
     - Tada!

** Angående studassbruk
   - Studass er ikke bare på sal for å godkjenne
   - Planlegg gjerne å jobbe med øvingen på sal til saltider
     - Kanskje kan du legge all ITGK-jobbing til saltider?
   - Det er travelt de siste timene
     - Bør være klar for innlevering på dette tidspunktet

** Angående mentalitet
   - Programmering handler i hovedsak _ikke_ om programmeringsspråket
     - men om /problemløsning/
   
   - Oppgave: Løs problem vha programmering.


*** Feil fokus                                                :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :BEAMER_env: block
    :END:
    [[file:hva-skal-jeg-skrive.png]]

*** Rett fokus                                                :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
#+ATTR_LATEX: :height 0.5\textheight
    [[file:hvordan-lose-problemet.png]]

***                                                                  :B_note:
    :PROPERTIES:
    :BEAMER_env: note
    :END:
   - Du får et problem
   - Oppgaven er å løse problemet vha programmering

   - Feil fokus: Hvordan skriver jeg rett ting til greia så dingsen blir grønn??
   - Rett fokus: _Hvordan ville jeg løse denne typen problem?_
       - Refleksjon rundt egen fremgangsmåte
         - Hvordan forklare til noen andre hvordan løse problemet?
       - DERETTER: Hvordan beskrive hvert steg mot løsningen vha kode?

   - Ingen grunn til å bli skremt av datamaskinen
     - Man kommer langt med problemløsning
    
   - Dog må det riktignok også sies:
     - Man blir flinkere til å løse problemer vha matlab/python hvis man snakker flytende matlab/python
     - Å løse problemene krever at man klarer å koble delsteg til programmeringsspråkkonstruksjoner
     - I starten vil problemene være på grunnivå og hovedsakelig kreve programmmeringsspråkskunnskap.
       - Hvordan skrive ut et tall 
         - disp(x)
     - Senere blir problemene mer involvert
       - Hvordan skrive ut minste tall i en liste med tall?
         Løse problemet:
         1. Finn minste tall i lista
            1. ...
         2. Skriv ut tallet
            - disp(x)


* Øving 1-materiale

** Matlab syntaks
   - Ikke så mye problemløsning enda
     - Man må vite hva en hammer er før man kan bygge hus

   - <2-> (Start Kahoot!)

** Matteuttrykk
   - <1-> Operatorer

     | =+, -= | =1 + 6 - 7= = $1 + 6 - 7$ = $0$            \pause  |
     |        |                                                    |
     | =*, /= | =2 * 3 / 4= = $\frac{2 \cdot 3}{4}$ = $1.5$ \pause |
     |        |                                                    |
     | =^=    | =2^3= = $2^3$ = $8$                        \pause  |

   - <2-> Presedens
     - Hva regnes ut først
     - Parenteser regnes ut /aller/ først

** Eksempler
   | =2 + 2=       \pause |  4 \pause |
   | =2 + 2 * 3=   \pause |  8 \pause |
   | =(2 + 2) * 3= \pause | 12 \pause |
   | =3 * 4 / 2=   \pause |  6 \pause |
   | =3 * (4 / 2)= \pause |  6 \pause |
   | =2^4=         \pause | 16 \pause |
   | =2 ^ 3+1=     \pause |  9 \pause |
   | =2^(3 + 1)=   \pause | 16 |

** Eksempeloppgave
   1. Hvor mange Fahrenheit tilsvarer $20 \degree C$?
      - Formelen er $\frac{9}{5} \cdot celsius + 32$

   2. Hva er stigningstallet til linjen gjennom (1, 3) og (3, -7)?
      - Formelen er $\frac{y_2 - y_1}{x_2 - x_1}$
   3. <2|visible@2> Kahoot spørsmål 1-3

** Variabler
   - Navngitte verdier
     - Navn av bokstaver, tall, og understrek.
   - Kan slå opp verdien ved å skrive navnet
   - Kan siden endre oppslaget

** Variabeltilordning
   - Syntaks: =<variabelnavn> = <uttrykk>=
     1. Først evalueres uttrykket
     2. Deretter opprettes variabeltilordningen

** Variabeltilordning
   [[file:Variabeltilordning.png]]

   - (Strengt tatt altså navngitt minne)
** Eksempeloppgave
   - Opprett variabelen =radius= med verdien 3

   - Regn ut volumet av en kule med radius 3, ved å bruke variabelen =radius=.
     - Formelen er $\frac{4}{3} \pi radius^3$
     
   - Opprett variabelen =V_kule= til å holde volumet.

   - Doble verdien av variablen =V_kule=.

   - Null ut alle definerte variabler.

   - <2|visible@2> Kahoot spørsmål 4

** Vektorer
   - For å holde en liste med verdier, kan man bruke vektorer
     - =[1, 2, 3, 4, 5]=
     - Kan opprettes vha kolon: =1:5=
     - Kan oppgi steglengde: =1:2:10=
   - <2> Eksempel: =radiuser = 1:10=
   - <2> Hent ut radius nr. 5
     - =radiuser(5)=
   - <2> Hent ut siste radius
     - =radiuser(end)=
     - Evt. =radiuser(length(radiuser))=

** Eksempeloppgave
- Lag en vektor med de fem første positive oddetallene. Lagre den i
  variabelen =v1=.
- Lag en vektor med heltallene fra 10 ned til 0, i den
  rekkefølgen. Lagre den i variabelen =v2=.
- Lag en vektor med tallene [0.1, 0.2, ..., 1.0], og lagre den i
  variablen =v3=.
- Summer de siste elementverdiene i vektorene =v1=, =v2= og =v3=
- <2> Kahoot spørsmål 5

** Operasjoner på hele vektorer
- Man kan utføre aritmetiske uttrykk også med vektorer
  - =[1, 2, 3] * 2 = [2, 4, 6]=
  - =[1, 2] + [2, 1] = [3, 3]=
- Til vanlig er operatorene matriseoperasjoner
  - Operandene må ha dimensjoner som passer
  - Mer om dette neste uke

- For å gjøre en operasjon per element, bruk =.<op>= (som =.*=, =./=, =.^=)

- <2> Eksempel:
  | Uttrykk                      | Resultat      |
  |------------------------------+---------------|
  | =[ 1, 2, 3 ] .* [ 3, 2, 1 ]= | =[ 3, 4, 3 ]= |
  | =[ 1, 2, 3 ] .^ 2=           | =[ 1, 4, 9 ]= |
  | =12 ./ [ 2, 3, 6 ]=          | =[ 6, 4, 2 ]= |

** Eksempeloppgave
- Regn ut $\frac{1}{9}$, $\frac{2}{9}$ og $\frac{3}{9}$ i én operasjon

- Regn ut $\frac{9}{1}$, $\frac{9}{2}$ og $\frac{9}{3}$ i én operasjon
  
- <2-> Regn ut tyngdekraften som virker på fire forskjellige astronauter på
  forskjellige planeter.
  - Opprett variabelen =astronautvekt = [63, 70, 83, 50]=
  - Opprett variabelen =planet_g = [9.81, 3.71, 1.62, 24.79]=
  - Regn ut variabelen =tyngdekraft=

- <3-> Kahoot spørsmål 6 og 7

** Areal av sylinder
   - Formel: $2 \pi r h + 2 \pi r^2$
   - Oppgave: regn ut areal for sylindrene med
     - radius = 3, høyde = 7
     - radius = 1, høyde = 8
     - radius = 9, høyde = 3
     (Hint: bruk elementvise operasjoner)

** Funksjoner: unngå repetisjon
   - Hva hvis vi trenger arealformelen flere steder?
   - Lag en funksjon
   - Gir òg mening som matematisk funksjon
     - $A_{sylinder}(radius, hoyde) = 2 \pi \cdot radius \cdot hoyde + 2 \pi \cdot radius^2$

** Egne funksjoner
   - Opprett ny fil
     - /funksjonsnavn/.m

#+begin_src matlab
function returverdi = funksjonsnavn(parameter1, parameter2)
    <kode>
end
#+end_src

** Oppgave
   1. Opprett en funksjon for å regne ut overflatearealet av en sylinder
      - $A_{sylinder}(r, h) = 2 \pi r h + 2 \pi  r^2$

   2. Opprett en funksjon for å regne ut volumet av en sylinder
      - $V_{sylinder}(r, h) = \pi r^2 h$

   3. Opprett en funksjon som skriver ut volumet og arealet av en sylinder
      - Trenger ikke returverdi

   4. <2-> Kahootspørsmål 8
        
** Finnes mange funksjoner fra før
   - =sum(), length(), min(), max(), mean(), median()=
   - =isprime()=
   - =disp(), input(), fprintf()=
   - =sqrt(), nthroot()=
   - =log(), log10(), sin(), cos(), atan()=
   - =integral()=

   - Vil presentes etter behov.
   - Bruk =help <funksjonsnavn>=

** Om innebygde funksjoner og variabelnavn
- Vær forsiktig med hva du kaller dine variabler
 
- Eksempel: hva skjer her?

  #+begin_src matlab
%% I kommandovinduet
disp(sum(1:5))    % Skriver ut 15
sum = 1 + 2 + 3     
disp(sum(1:5))    % Huffda
clear sum         % Løser problemet
  #+end_src

** Innputt og utputt
- For å kommunisere med brukeren av programmet, kan vi bruke
  funksjonene =disp()= og =input()=

- Oppgave: lag en funksjon som leser inn et tall fra brukeren, og
  skriver ut det dobbelte.

- Oppgave: lag en funksjon som ber brukeren skrive inn høyden og
  radiusen til en sylinder, og som skriver ut volumet og arealet til
  sylinderen.

** Globale og lokale variabler
- Variabler laget i en funksjon eksiterer bare for den funksjonen.

#+begin_src matlab
function y = f(x) % Lag lokal variabel 'x'
    z = x + x;    % Lag lokal variabel 'z'
    y = z * z;    % Lag lokal variabel 'y'
end

%% I kommandovinduet
x = 1;              
y = 2;              
z = 3;              
disp(input('Skriv inn et tall'));         
disp([x, y, z]);    % Skriver ut "1 2 3" - de lokale
                    % variablene til f er ikke tilgjengelige!
#+end_src


** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-7-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-6-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-5-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-4-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-3-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-2-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2-1-left.pdf]]
** Forskjell på retur og utskrift
[[file:figures/forskjell-utskrift-retur2.pdf]]


** Bonus: Funksjonskall uten parenteser
   - Sender parametrene som strenger

   - Sammenlikn:
     - =help sum=
     - =help(sum)=
     - =help('sum')=




* Kladd                                                              :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
- Bør spørre om de vil gå gjennom øving for:
  - Denne uken
  - Neste uke

- Gjøre forskjell på tirsdag-onsdag-torsdagsparalleller?








# Local Variables:
# eval: (flyspell-mode -1)
# End:
