# -*- org-beamer-outline-frame-title:  Innhold -*-

#+TITLE:     Øvingsforelesning TDT4105
#+AUTHOR:    Benjamin A. Bjørnseth
#+EMAIL:     benjambj@idi.ntnu.no
#+DATE:      \today{}
#+DESCRIPTION: Nest siste øvingsforelesning matlab. Tema etter ønske.
#+KEYWORDS: matlab tdt4105 etter ønske.

#+LANGUAGE:  no
#+latex_header: \usepackage[norsk]{babel}
#+BEAMER_THEME: ntnubokmaal

#+latex_header: \subtitle{Eksamensoppgaver, filbehandling.}

#+STARTUP: beamer

#+LATEX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation,smaller]

#+OPTIONS:   H:2 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:nil mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME:
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

#+latex_header: \renewcommand\maketitle{\ntnutitlepage}
#+latex_header: \renewcommand\alert{\textbf}


#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsection]\end{frame}}
#+latex_header: \AtBeginSubsection[]{\begin{frame}<beamer>\frametitle{Oversikt}\tableofcontents[currentsubsection]\end{frame}}

#+latex_header: \usepackage{minted}




* Ønskede tema
** Ønskeoversikt
1. Vanskelige eksamensoppgaver
2. Structs
3. Funksjoner
4. Preallokering
5. Enkle eksamensoppgaver


* Ordinær eksamen 2013, oppg. 4

** Oppg. 4a): Programmeringslogikk.

** Oppg. 4b): Fil-lesing, while-løkke, =strcmp()=

** +Oppg. 4c): =fprintf()=+ 

- Hopper over denne, laber interesse for formatert utskrift.

** Oppg. 4d): Filskriving.

** Oppg. 4e): Menystyrt, større program, fil-lesing, structs.

* Ordinær eksamen 2013, oppg. 3c)

** Kodeforståelse, rekursjon

* Kont 2015, oppg. 4

** Oppg. 4a): =input()=, cellelister

** Oppg. 4b): cellelister, for-løkke, =strcmp()= 

** Oppg. 4c): =input()=, while-løkke, logiske betingelser

** Oppg. 4d): for-løkke.

** Oppg. 4e): for-løkke, celleliste.

** Oppg. 4f): Fil-lesing, cellelister, større program.

* Ordinær eksamen 2014, oppg. 2

** Oppg. 2a): =input()=, structs

** Oppg. 2b): structs, fil-lesing. 

** Oppg. 2c): =fprintf()=, for-løkke, structs.

** Oppg. 2d): Filskriving, =fprintf()=, struct-array.

** Oppg. 2e): Programmeringslogikk.

# Local Variables:
# eval: (flyspell-mode -1)
# End:
