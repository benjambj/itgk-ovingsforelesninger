function resultat = bridgePoints(melding, antallStikk)
    trekk = melding(1) - '0';
    trumf = melding(3:end);
    klarteTrekk = antallStikk - 6;
    if klarteTrekk < trekk
        resultat = -50 * (trekk - klarteTrekk);
        return;
    end
    
    if vellykketUtgang(melding, antallStikk)
        bonus = 300;
    else
        bonus = 50;
    end
    switch trumf
        case 'grand'
            bonus = bonus + 10;
            poengPerTrekk = 30;
        case {'spar', 'hjerter'}
           	poengPerTrekk = 30;
        otherwise
            poengPerTrekk = 20;
    end
    resultat = klarteTrekk * poengPerTrekk + bonus;
end