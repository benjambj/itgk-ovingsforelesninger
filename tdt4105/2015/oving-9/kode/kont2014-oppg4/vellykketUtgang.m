function resultat = vellykketUtgang(melding, antallStikk)
    resultat = bidOk(melding, antallStikk);
    if ~resultat
        return;
    end
    
    trumf = melding(3:end);
    trekk = melding(1) - '0';
    switch trumf
        case 'grand'
            resultat = trekk >= 3;
        case {'spar', 'hjerter'}
            resultat = trekk >= 4;
        otherwise
            resultat = trekk >= 5;
    end
end