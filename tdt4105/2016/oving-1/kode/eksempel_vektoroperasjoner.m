% Oppgave 1
% Regn ut 1/9, 2/9 og 3/9 i �n operasjon
[1, 2, 3] / 9

% Oppgave 2
% Regn ut 9/1, 9/2 og 9/3 i �n operasjon
9 ./ [1, 2, 3]
% Kommentar: Her vil det ikke fungere � skrive 9 / [1, 2, 3]. Dette er
% fordi '/' har en spesiell betydning for matriser: B / A gir l�sningen x
% for likningssystemet xA = B, jamf�r "help /". 
% I det f�rste eksempelet g�r dette fint, fordi:
%   - Vi skal regne ut B/A, hvor B = [1, 2, 3] og A = 9;
%   - Dette tilsvarer � l�se likningssystemet x * 9 = [1, 2, 3] for x;
%   - Dette systemet har l�sningen x = [1/9, 2/9, 3/9]
% I det andre eksempelet g�r det ikke fint, fordi:
%   - Vi skal regne ut B/A, hvor B = 9 og A = [1, 2, 3];
%   - Dette tilsvarer � l�se likningssystemet x * [1, 2, 3] = 9;
%   - Siden siste faktor i multiplikasjonen har tre kolonner, m� resultatet
%     n�dvendigvis ogs� ha tre kolonner (det er slik matrisemultiplikasjon
%     fungerer); 
%   - Vi sier til matlab at svaret skal ha �n kolonne. Matrisedimensjonene
%     er alts� ikke forenelige, og matlab klager med meldingen "Matrix
%     dimensions must agree"
%
% L�sningen er heldigvis s�re enkel: bruk den elementvise operasjon "./".
 
% Oppgave 3: Regn ut tyngdekraften som virker p� astronautene.
astronautvekt = [63, 70, 83, 50]
planet_g = [ 9.81, 3.71, 1.62, 24.79 ]
tyngdekraft = astronautvekt .* planet_g
% Kommentar: Igjen vil det ikke fungere med den vanlige '*'-operatoren,
% fordi:
%    1. For det f�rste gj�r ikke matrisemultiplikasjon det vi vil (det er 
%       ikke elementvis multiplikasjon), s� selv om Matlab hadde akseptert 
%       uttrykket ville svaret v�rt feil.
%    2. Matrisemultiplikasjon A*B krever at A har like mange kolonner som B
%       har rader. Det er ikke tilfellet for oss. Derfor gir Matlab
%       feilmeldingen "Inner matrix dimensions must agree" hvis vi pr�ver �
%       skrive "astronautvekt * planet_g".
