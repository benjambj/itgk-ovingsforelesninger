% Oppgave 1
% Oppretter variablene radius med verdien 3
radius = 3

% Oppgave 2
% Regner ut volumet av en kule med radius 3 vha variabelen 'radius',
% og den innebygde variabelen 'pi'
4/3 * pi * radius ^ 3

% Oppgave 3
% Tilordner forrige resultat til variabelen V_kule
V_kule = ans
% Kommentar: man �nsker ikke vanligvis � bruke variabelen 'ans' slik. I
% stedet �nsker man � skrive "V_kule = 4/3 * pi * radius ^ 3" direkte. Det
% kan imidlertid v�re praktisk � bruke 'ans' n�r man eksperimenter i
% kommandovinduet, hvis man �nsker � ta vare p� midlertidige beregninger
% man gj�r.

% Oppgave 4
% Doble verdien av variabelen V_kule
V_kule = 2 * V_kule

% Oppgave 5
% Null ut alle definerte variabler
clear
% 'clear'-funksjonen kan ogs� bruker til � nulle ut spesifikke variabler.
% Eksempelvis: "clear V_kule" vil slette variabelbindingen fra V_kule.
