function json = person_til_json( person )
    json = sprintf('{"navn" : "%s", "alder" : %d, "adresse" : {"gate" : "%s", "nr" : %d, "postnr" : %d}}', ...
        person.navn, person.alder, person.adresse.gate, person.adresse.nr, person.adresse.postnr);
end

