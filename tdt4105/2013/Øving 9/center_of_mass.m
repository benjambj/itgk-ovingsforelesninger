function center = center_of_mass(stang)

totalVekt = sum(stang);
likeVekt = totalVekt/2;
vekt = 0;

% Finn posisjonen i, der vi har minst likeVekt mye vekt på venstre side
for i = 1:length(stang)    
    if vekt + stang(i) >= likeVekt
        break;
    else
        vekt = vekt + stang(i);
    end
end

% Regner ut nøyaktig posisjon
center = (i-1) + (likeVekt-vekt) / stang(i);

end

