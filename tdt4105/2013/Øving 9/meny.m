function meny()
    while true
        fprintf('\nMeny\n-----------\n1. Skriv nytt tall til databasen\n2. Vis alle tall i databasen\n3. Slett databasen\n0. Avslutt\n\n');
        valg = input('Oppgi ditt valg: ');
        switch valg
            case 0
                break;
            case 1
                tall = input('Oppgi tallet som skal skrives til databasen: ');
                skrivTilFil('data.txt', tall);
                fprintf('\nUtført!\n');
            case 2
                data = lesFraFil('data.txt');
                if isempty(data)
                    disp('Databasen er tom!');
                else
                    fprintf('Innholdet i databasen er:');
                    disp(data);                
                end                
            case 3
                renskFil('data.txt');
                fprintf('Databasen er slettet!');
        end
    end
end

