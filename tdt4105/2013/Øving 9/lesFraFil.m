function data = lesFraFil( filnavn )

    fil = fopen(filnavn, 'r');
    i = 1;
    linje = fgetl(fil);
    data = [];
    while linje ~= -1 % sjekker om vi er nådd siste linje. fgetl returnerer -1 når vi har nådd siste linje
        data(i) = str2num(linje); % gjør om tekststrengen til et tall
        linje = fgetl(fil); % hent ny linje
        i = i + 1;
    end
    fclose(file);

end

