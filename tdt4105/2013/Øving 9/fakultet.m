function res = fakultet(n)
    if n == 0
        res = 1;
    else
        res = n*fakultet(n-1);
    end
end

