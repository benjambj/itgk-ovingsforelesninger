function sum = listeSum(liste)
    % en rekursiv måte å regne ut summen av en liste
    sum = liste(1);
    if length(liste) > 1
        sum = sum + listeSum(liste(2:end));
    end
end