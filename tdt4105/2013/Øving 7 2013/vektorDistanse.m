function distance = vektorDistanse(q, p)
    
sum = 0;
for i = 1:length(q)
    sum = sum + (q(i)-p(i))^2;
end
distance = sqrt(sum);  
    
% kan også gjøres uten for løkke:
% distance = sqrt(sum((q-p).^2));
end

