function [x, y] = sirkel(antallPunkter)

for i = 1:(antallPunkter+1)
    vinkel = 2*pi*(i-1)/antallPunkter;
    x(i) = cos(vinkel);
    y(i) = sin(vinkel);
end

% Kan også gjøres uten for løkker:
% punkter = 1:antallPunkter;
% vinkler = 2*pi*(punkter-1)./antallPunkter;
% x = cos(vinkler);
% y = sin(vinkler);

end

% Brukes på følgende måte:
% [x, y] = sirkel(30);
% plot(x, y);
