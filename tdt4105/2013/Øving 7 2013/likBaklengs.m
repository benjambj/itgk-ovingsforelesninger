function resultat = likBaklengs(a, b)

    n = length(a);
    if n ~= length(b)
        resultat = false;
    else
        resultat = true;
        for i = 1:n
            if a(i) ~= b(n-i+1)
                resultat = false;
            end
        end
    end
    
end

