function r = fu1(a)

r = 0;
while a > 0
    s = rem(a,10);
    r = r+s;
    a = (a-s)/10;
end

% a = 1234
% r = 0

% Første iterasjon
% 1234 / 10 = 123.4 Resten er 0.4*10 = 4
% s = 4;
% r = r + 4 = 4;
% a = (1234-4)/10 = 123

% Neste iterasjon:
% 123 / 10 = 12.3 Resten er 0.3*10 = 3
% s = 3;
% r = r + 3 = 7;
% a = (123-3)/10 = 12

% Neste iterasjon:
% 12 / 10 = 1.2 Resten er 0.2*10 = 2
% s = 2;
% r = r + 2 = 9;
% a = (12-2)/10 = 1

% Neste iterasjon
% 1 / 10 = 0.1 Resten er 0.1*10 = 1
% s = 1;
% r = r + 1 = 10;
% a = (1-1)/10 = 0;

% While løkken avslutter fordi a er ikke lenger større en 0
% Svaret er r = 10;

end

