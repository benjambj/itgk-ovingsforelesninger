function outList = noVowels(inList)

for i = 1:length(inList)
    ord = inList{i};
    nyttOrd = [];
    
    % En for løkke som går gjennom hvert tegn i variabelen ord
    for j = 1:length(ord)
        tegn = ord(j);
% Kan gjøres med en switch:
%         switch tegn
%             case {'a', 'e', 'i', 'o', 'u', 'y'}
%                 % Gjør ingenting, vi ønsker ikke legge til vokaler
%             otherwise
%                 % Bokstaven er ikke en vokal, legg til tegn til
%                 % tekststrengen nyttOrd
%                 nyttOrd = [nyttOrd tegn];
%         end
% Eller en if setning:
        if tegn ~= 'a' && tegn ~= 'e' && tegn ~= 'i' && tegn ~= 'o' && tegn ~= 'u' && tegn ~= 'y'
            % Vi vet her at tegn ikke er en vokel; vi kan til tegn til
            % nyttOrd
            nyttOrd = [nyttOrd tegn];
        end
    end
    % Ersatt element i med det nye ordet nyttOrd
    outList{i} = nyttOrd;
end

end

