function [gjennomsnitt, min, max] = pulsStatistikk(pulsData)

min = pulsData(1);
max = pulsData(1);
sum = 0;
for i = 1:length(pulsData)
    sum = sum + pulsData(i);
    if pulsData(i) < min
        min = pulsData(i);
    end
    if pulsData(i) > max
        max = pulsData(i);
    end
end

gjennomsnitt = sum / length(pulsData);


end

