tall = input('Oppgi neste tall eller skriv avslutt: ', 's');
liste = [];
i = 1;
while strcmp(tall, 'avslutt') == false
    if isempty(str2num(tall))
        fprintf('Du må skrive inn et tall eller teksten avslutt!\n');
    else
        liste(i) = str2num(tall);
        i = i + 1;
    end
	tall = input('Oppgi neste tall eller skriv avslutt: ', 's');
end
fprintf('Din liste ser slik ut:\n');
disp(liste);
