function decimal = hex2decimal(hex)
    decimal = 0;
    for i = 1:length(hex)
        switch hex(i)
            case '0'
                number = 0;
            case '1'
                number = 1;
            case '2'
                number = 2;
            case '3'
                number = 3;
            case '4'
                number = 4;
            case '5'
                number = 5;
            case '6'
                number = 6;
            case '7'
                number = 7;
            case '8'
                number = 8;
            case '9'
                number = 9;
            case 'a'
                number = 10;
            case 'b'
                number = 11;
            case 'c'
                number = 12;
            case 'd'
                number = 13;
            case 'e'
                number = 14;
            case 'f'
                number = 15;
        end                       
        
        decimal = decimal + number*16^(length(hex)-i);
    end
end