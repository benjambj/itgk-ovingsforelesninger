function list = summerOlympics2(firstYear, lastYear)

i = 1;
% Går gjennom alle OL år fra 1948 til lastYear og legger til alle OL år som er etter firstYear i listen list
for year = 1948:4:lastYear
    if year >= firstYear
        list(i) = year;
        i = i + 1;
    end
end

end
