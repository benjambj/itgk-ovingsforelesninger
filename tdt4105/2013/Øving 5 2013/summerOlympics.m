function list = summerOlympics(firstYear, lastYear)

% Finne ut hvilket år som er det første OL året ETTER firstYear
for year = 1948:4:lastYear
    if year >= firstYear
        startYear = year;
        break;
    end
end

% startYear inneholder nå det første OL året mellom firstYear og lastYear
% Genererer listen over alle OL år fra og med startYear til lastYear
list = startYear:4:lastYear;

end

