function decimal = binary2decimal(binary)
    decimal = 0;
    for i = 1:length(binary)
        if binary(i) == '1'
            decimal = decimal + 2^(length(binary)-i);
        end
    end
end