function [ liste1, liste2 ] = split( liste, n )

    liste1 = liste(1:n);
    liste2 = liste(n+1:end);

end

