function evaluer(funksjon, a, b, n )

    h = (b-a) / n;

    for x = a:h:b
        fprintf('f(%f) = %f\n', x, funksjon(x));        
    end

end

