function list = cap_data(list, min_value, max_value)

for i = 1:length(list)
    if list(i) < min_value
        list(i) = min_value;
    elseif list(i) > max_value
        list(i) = max_value;
    end
end

end
