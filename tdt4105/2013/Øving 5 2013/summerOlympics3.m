function list = summerOlympics3(firstYear, lastYear)

% Regner oss frem til første OL år etter firstYear
if rem(firstYear, 4) == 0
    startYear = firstYear;
else
    startYear = firstYear + 4 - rem(firstYear, 4);
end
list = startYear:4:lastYear;

end
