function V = kjegleVolum(r, h)

    V = pi * r^2 * h / 3;

end

