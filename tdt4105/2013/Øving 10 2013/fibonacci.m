function resultat = fibonacci( n )

if n <= 0
    resultat = 0;
elseif n == 1
    resultat = 1;
else
    resultat = fibonacci(n-1) + fibonacci(n-2);
end

end

