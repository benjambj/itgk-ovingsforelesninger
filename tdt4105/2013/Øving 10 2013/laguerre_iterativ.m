function resultat = laguerre_iterativ(n, x)

% iterativ løsning på laguerre oppgaven

l = zeros(1, n+1);
l(1) = 1; % for n = 0
l(2) = 1-x; % for n = 1

for i = 3:n+1
    l(i) = (2*(i-1)-1-x)*l(i-1) - ((i-1)-1)^2*l(i-2); % for n = i-1
end

resultat = l(n+1); % henter ut resultat for n

end

