function resultat = laguerre(n, x)

    if n <= 0
        resultat = 1;
    elseif n == 1
        resultat = 1-x;
    else
        resultat = (2*n-1-x)*laguerre(n-1,x) - (n-1)^2*laguerre(n-2,x);
    end

end

