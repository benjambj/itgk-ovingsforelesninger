function list = create_db(temp, rain, humidity, wind)

% Antar at alle listene har lik lengde
for i = 1:length(temp)
    list(i) = struct('temp', temp(i), 'rain', rain(i), 'humidity', humidity(i), 'wind', wind(i));
end

end

