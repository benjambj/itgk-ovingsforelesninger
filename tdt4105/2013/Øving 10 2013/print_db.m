function print_db(weather)

fprintf('Day | Temp | Rain | Humidity | Wind\n');
fprintf('====+======+======+==========+======\n');
for i = 1:length(weather)
    day = weather(i);
    fprintf('%4d %6d %6d %10d %6d\n', i, day.temp, day.rain, day.humidity, day.wind);
end

end

