function lengde = vektorLengde(vektor)
lengde = 0;
for e = vektor
    lengde = lengde + e^2;
end
lengde = sqrt(lengde);

end
