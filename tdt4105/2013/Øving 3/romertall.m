function bokstav = romertall(tall)
    switch tall
        case 0
            bokstav = '';
        case 1
            bokstav = 'I';
        case 2
            bokstav = 'II';
        case 3
            bokstav = 'III';
        case 4
            bokstav = 'IV';
        case 5
            bokstav = 'V';
        case 6
            bokstav = 'VI';
        case 7
            bokstav = 'VII';
        case 8
            bokstav = 'VIII';
        case 9
            bokstav = 'IX';
        case 10
            bokstav = 'X';
        otherwise
            bokstav =  'Number must be from 0 to 10';
    end 
end

