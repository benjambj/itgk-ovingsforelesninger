function lengde = vektorlengde(vektor)
    lengde = 0;
    for l = vektor
        lengde = lengde + l^2;
    end
    lengde = sqrt(lengde);
end