function resultat = fra_romertall(romertall)
    resultat = 0;
    if strcmp(romertall, 'nulla')
        return;
    end
    n = length(romertall);
    for i = 1:n
        switch romertall(i)
            case 'I'
                if i < n && (romertall(i+1) == 'V' || romertall(i+1) == 'X')
                    resultat = resultat - 1;
                else
                    resultat = resultat + 1;
                end
            case 'V'
                resultat = resultat + 5;
            case 'X'
                if i < n && (romertall(i+1) == 'L' || romertall(i+1) == 'C')
                    resultat = resultat - 10;
                else
                    resultat = resultat + 10;
                end
            case 'L'
                resultat = resultat + 50;
            case 'C'
                if i < n && (romertall(i+1) == 'D' || romertall(i+1) == 'M')
                    resultat = resultat - 100;
                else
                    resultat = resultat + 100;
                end
            case 'D'
                resultat = resultat + 500;
            case 'M'
                resultat = resultat + 1000;
            otherwise
                error('Ugyldig romertall: ukjent siffer "%c"', romertall(i));
        end
    end
end

