function resultat = romertall(tall)

switch tall
    case 0
        resultat = 'nulla';
    case 1
        resultat = 'I';
    case 2
        resultat = 'II';
    case 3
        resultat = 'III';
    case 4
        resultat = 'IV';
    case 5
        resultat = 'V';
    case 6
        resultat = 'VI';
    case 7
        resultat = 'VII';
    case 8
        resultat = 'VIII';
    case 9
        resultat = 'IX';
    case 10
        resultat = 'X';
    otherwise
        error('Kun tall mellom 0 og 10 er st�ttet. Fikk inn %d', tall);       
end

%% Alternativ
% switch tall
%     case 0
%         resultat = 'nulla';
%     case {1, 2, 3}
%         resultat = repmat('I', 1, tall);
%     case {5, 6, 7, 8}
%         resultat = ['V', romertall(tall - 5)];
%     case {4, 9}
%         resultat = ['I', romertall(tall + 1)];
%     case 10
%         resultat = 'X';
%     otherwise
%         error('Kun tall mellom 0 og 10 er st�ttet. Fikk inn %d', tall);            
% end

end