function velg_spill()
    disp('Hva vil du spille?');
    disp('(1) Tripp trapp tresko');
    disp('(2) Sjakk');
    disp('(3) Ludo');
    disp('(4) Poker');
    valg = input('');
    switch valg
        case 1
            spill_tripp_trapp_tresko();
        case 2
            spill_sjakk();
        case 3
            spill_ludo();
        case 4
            spill_poker();
        otherwise
            disp('Ukjent valg');
    end
            
         
        
end