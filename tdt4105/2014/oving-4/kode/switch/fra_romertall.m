function resultat = fra_romertall(romertall)
    switch romertall
        case 'nulla'
            resultat = 0;
        case 'I'
            resultat = 1;
        case 'II'
            resultat = 2;
        case 'III'
            resultat = 3;
        case 'IV'
            resultat = 4;
        case 'V'
            resultat = 5;
        case 'VI'
            resultat = 6;
        case 'VII'
            resultat = 7;
        case 'VIII'
            resultat = 8;
        case 'IX'
            resultat = 9;
        case 'X'
            resultat = 10;
        otherwise
            error('Ukjent romertall "%s"', romertall);
    end
end