function skriv_ut_dato(dag, maaned, aar, bruk_norsk_format)
    if bruk_norsk_format
        fprintf('%02d.%02d.%04d\n', dag, maaned, aar);
    else
        fprintf('%04d-%02d-%02d\n', aar, maaned, dag);
    end
end