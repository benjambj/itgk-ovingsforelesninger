function rot = kvadratrot(tall)
    x_ny = tall/2;
    for i = 1:20
        x_gammel = x_ny;
        x_ny = x_gammel - (x_gammel^2 - tall) / (2*x_gammel);
        fprintf('Iterasjon %d:\tx_%d = %.20f, x_%d = %.20f\n', ...
                i, i-1, x_gammel, i, x_ny);
    end
    rot = x_ny;
end