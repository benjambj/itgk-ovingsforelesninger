%% Demonstrerer direkte konstruksjon, og indeksering med posisjonsvektor.
% I oppgaven skal vi ta to lister, og returnere de to listene stokket om p�
% samme vis.
%
% Listene skal stokkes om p� samme vis, hvilket betyr at vi �nsker at
% elementene p� plass 'i' i de opprinnelige vektorene skal v�re p� samme
% plass i de resulterene vektorene. Vi vil ogs� ha med alle element. Vi
% trenger derfor en permutasjon, alts� en vilk�rlig ordnet liste av tallene
% fra og med 1 til og med lengden av listene. Gitt lister av lengde 4, kan
% en eksempelpermutasjon v�re [1, 4, 2, 3]. Det g�r an � la permutasjonen
% v�re implisitt gitt av hvordan vi til slutt ender opp med � stokke
% vektoren, men hvis vi lager en gitt permutasjon og bruker indeksering i
% hver liste vil vi f� det resultatet vi �nsker oss direkte. 
% Eksempel:
%              liste = { 'en', 'god', 'vin', 'r�d' }
%        permutasjon = [    1,     4,     2,     3 ]
% --------------------------------------------
% liste(permutasjon) = { 'en', 'r�d', 'god', 'vin' }
%
% Siden vi skal returnere to lister, b�r vi igjen vurdere preallokering.
% Siden indekseringen gir oss resultatlisten vi �nsker oss direkte, trenger
% vi imidlertid ikke preallokere eksplisitt. Vi konstruerer hele listen
% direkte (med en gang), og matlab h�ndterer da allokering av nok minne.
function [newListOne, newListTwo] = randomSequence(listOne, listTwo)
    permutation = generatePermutation(length(listOne))
    newListOne = listOne(permutation);
    newListTwo = listTwo(permutation);
end

% Denne funksjonen genererer en permutasjon vi kan brukere for � stokke om
% p� listene. Her skal vi returnere en liste av tall - og her preallokerer
% vi, siden vi vet hvor mange listelement vi skal ha og ikke vet om noen
% m�te � generere en slik permutasjon direkte.
%  (med mindre vi vet om matlab-funksjonen randperm...)
function resultat = generatePermutation(n)
    tall = 1:n;
    resultat = zeros(1, n);
    for i = 1:n
       permutedIndex = randi(length(tall) - i + 1);
       resultat(i) = tall(permutedIndex);
       t = tall(end - i + 1);
       tall(end - i + 1) = tall(permutedIndex);
       tall(permutedIndex) = t;
    end
end