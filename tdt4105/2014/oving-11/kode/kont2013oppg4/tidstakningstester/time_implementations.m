function res = time_implementations(impls, varargin)
    assert(iscell(impls));
    mintime = 1e40;
    for i = 1:length(impls)
        fprintf('Timing function %s... \n', func2str(impls{i}));
        tic;
        impls{i}(varargin{:});
        time = toc;
        fprintf('Time: %f seconds\n', time);
        if time < mintime
            res = impls{i};
            mintime = time;
        end
    end 
end