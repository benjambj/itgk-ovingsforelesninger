function [newListOne, newListTwo] = randomSequence(listOne, listTwo)
    persistent impl;
    if isempty(impl)
        letters = 'a':'z';
        wordlength = 30;
        numberOfWords = 1e5;
        wordlist1 = arrayfun(...
            @(c) letters(randi(length(letters), 1, wordlength)), ...
            1:numberOfWords, 'UniformOutput', false);
        wordlist2 = arrayfun(...
            @(c) letters(randi(length(letters), 1, wordlength)), ...
            1:numberOfWords, 'UniformOutput', false);
        impl = time_implementations({@randomSequenceIndexing, ...
                                     @randomSequenceInplaceExchange, ...
                                     @randomSequenceIndexingWithBuiltin}, ...
                                     wordlist1, wordlist2); 
    end
    [newListOne, newListTwo] = impl(listOne, listTwo);
end

function [newListOne, newListTwo] = randomSequenceIndexing(listOne, listTwo)
    permutation = generatePermutation(length(listOne));
    newListOne = listOne(permutation);
    newListTwo = listTwo(permutation);
end

function [newListOne, newListTwo] = randomSequenceIndexingWithBuiltin(listOne, listTwo)
    permutation = randperm(length(listOne));
    newListOne = listOne(permutation);
    newListTwo= listTwo(permutation);
end

function resultat = generatePermutation(n)
    tall = 1:n;
    resultat = zeros(1, n);
    for i = 1:n
       permutedIndex = randi(length(tall) - i + 1);
       resultat(i) = tall(permutedIndex);
       t = tall(end - i + 1);
       tall(end - i + 1) = tall(permutedIndex);
       tall(permutedIndex) = t;
    end
end

function [listOne, listTwo] = randomSequenceInplaceExchange(listOne, listTwo)
    for i = 1:length(listOne)
        numCompleted = i - 1;
        newIndex = randi(length(listOne) - numCompleted) + numCompleted;
        t = listOne{i};
        listOne{i} = listOne{newIndex};
        listOne{newIndex} = t;
        t = listTwo{i};
        listTwo{i} = listTwo{newIndex};
        listTwo{newIndex} = t;
    end
end