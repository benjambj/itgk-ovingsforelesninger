disp('The NoVowels Game');
disp('=================');
disp('Player 2: Look away from the screen');
disp('Player 1: Write in a list of English words in lower-case');

wordList = enterWords();
noVowelsList = noVowels(wordList);

[answers, quizzes] = randomSequence(wordList, noVowelsList);

printNewLines(50);
disp('Player 2: Guess words that lack all vowels:');
points = playGame(answers, quizzes);
fprintf('You have got %d out of %d points\n', points, length(quizzes));