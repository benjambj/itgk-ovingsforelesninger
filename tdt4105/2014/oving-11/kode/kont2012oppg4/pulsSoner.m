function soner = pulsSoner(maksPuls, pulsData)
    grenser = pulsSoneGrenser(maksPuls);
    soner = zeros(1, length(grenser));
    antallAlleredeTalt = 0;
    for i = 1:length(grenser)
        sonenr = 6 - i;
        antallOverGrense = length(pulsData(pulsData >= grenser(sonenr)));
        soner(sonenr) = 100*(antallOverGrense - antallAlleredeTalt)/length(pulsData);
        antallAlleredeTalt = antallOverGrense;
    end
end