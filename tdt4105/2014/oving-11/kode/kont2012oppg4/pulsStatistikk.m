function [snittpuls, minpuls, makspuls] = pulsStatistikk(pulsData)
    minpuls = pulsData(1);
    makspuls = pulsData(1);
    sum = pulsData(1);
    for i = 2:length(pulsData)
        if pulsData(i) < minpuls
            minpuls = pulsData(i);
        end
        if pulsData(i) > makspuls
            makspuls = pulsData(i);
        end
        sum = sum + pulsData(i);
    end
    snittpuls = sum/ length(pulsData);
end