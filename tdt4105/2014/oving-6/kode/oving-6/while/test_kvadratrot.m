tolerance = 1e-6;
assert(abs(kvadratrot(1e-4) - sqrt(1e-4)) < tolerance);
assert(abs(kvadratrot(1) - sqrt(1)) < tolerance);
assert(abs(kvadratrot(2) - sqrt(2)) < tolerance);
assert(abs(kvadratrot(12931) - sqrt(12931)) < tolerance);

disp('All tester av kvadratrot() passerte');
