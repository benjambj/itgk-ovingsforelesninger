function plott_eksponensialfordeling()
    plot(-2:0.1:10, f(-2:0.1:10));
end

function resultat = f(x)
    resultat = exp(-x) .* (x >= 0);
end