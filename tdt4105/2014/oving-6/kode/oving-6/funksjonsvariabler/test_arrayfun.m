function test_arrayfun()
    disp('e .^ (1:5) regnet ut ved hjelp av h(x, i):');
    disp(arrayfun(@(x) sum(h(x, 0:100)), 1:5));
    disp('e .^ (1:5) regnet ut ved hjelp av matlabs exp-funksjon:');
    disp(exp(1:5));
end