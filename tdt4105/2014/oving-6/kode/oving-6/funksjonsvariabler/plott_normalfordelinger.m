function plott_normalfordelinger()
    hold on;
    verdiomraade = -5:0.1:5;
    plott_funksjon(normalfordeling(0, 0.2), verdiomraade);
    plott_funksjon(normalfordeling(0, 1), verdiomraade);
    plott_funksjon(normalfordeling(0, 5.0), verdiomraade);
    plott_funksjon(normalfordeling(-2, 0.5), verdiomraade);
end

function plott_funksjon(f, verdiomraade)
    plot(verdiomraade, f(verdiomraade));
end

function fordeling = normalfordeling(forventningsverdi, varians)
    u = forventningsverdi;
    s = sqrt(varians);
    fordeling = @(x) 1/(s*sqrt(2*pi)) * exp(-(x - u).^2/(2*s^2));
end