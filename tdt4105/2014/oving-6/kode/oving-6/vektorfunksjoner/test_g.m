assert(g(0) == f(0)^2);
assert(g(1) == f(1)^2);
assert(all(g(1:100) == arrayfun(@g, 1:100)));
disp('All tests of g() passed');