assert(f(0) == 0);
assert(f(1) == 2 * pi);
assert(all(f(1:100) == arrayfun(@f, 1:100)));
disp('All tests of f() passed');