function plot_out(outfile)
    plots = initialize_plot_var();
    currentTime = 0;

    fd = fopen(outfile);
    line = fgetl(fd);
    lineCount = 0;
    while ischar(line)
        lineCount = lineCount + 1;
        if is_index(line)
            plots = add_index(line, plots);
        elseif is_time(line)
            currentTime = next_time(line);
        else
            words = strsplit(line);
            if length(words) == 2
                index = str2double(words{1});
                value = str2double(words{2});
                if ~isnan(index) && ~isnan(value)
                    plots(index).i = plots(index).i + 1;
                    plots(index).data(1, plots(index).i) = currentTime;
                    plots(index).data(2, plots(index).i) = value;
                    % Skriv ut fremgang.
                    if mod(plots(index).i, 10000) == 0
                        fprintf('%d -> #%d\n', index, plots(index).i);
                        fflush(stdout);
                    end
                end
            end
        end
        line = fgetl(fd);
    end
    fclose(fd);
    fprintf('Read %d lines\n', lineCount);
    plot_all(plots);
end

function plots = initialize_plot_var()
    plots(1).data = zeros(2, 1e7);
    plots(1).i = 0;
    plots(1).name = '';
end


function res = is_index(line)
    res = strncmp(line, '.index', 6);
end

function plots = add_index(line, plots)
    words = strsplit(line);
    index = str2double(words{3});
    plots(index).name = words{2}(4:end-1);
    plots(index).i = 0;
    fprintf('Add var %s as index %d\n', plots(index).name, index);
    fflush(stdout);
end

function res = is_time(line)
    res = ~isnan(str2double(line));
end

function time = next_time(line)
    time = str2double(line);
end

function plot_all(plots)
    hold on;
    colors = 'krgbmc';
    for i = 1:length(plots)
        fprintf('Plotting %s\n', plots(i).name);
        plot_i = plots(i).i;
        semilogy(plots(i).data(1, 1:plot_i), plots(i).data(2, 1:plot_i), ...
             strcat(colors(mod(i-1, length(colors)) + 1), ';', strrep(plots(i).name, '_', '-'), ';'));
    end
end