function dekrypter_fil(kryptert_fil, dekryptert_fil, dekrypterings_info_fil)
    load(dekrypterings_info_fil);
    if ~exist('D', 'var') || ~exist('m', 'var')
        error('Ugyldig dekrypterings-info-fil');
    end
    
    % 1. �pne filene
    kryptert_fd = fopen(kryptert_fil, 'rb');
    dekryptert_fd = fopen(dekryptert_fil, 'wb');
        
    % 2. Bruk filene
    blokkstoerrelse = length(D);
    [blokk, n] = fread(kryptert_fd, blokkstoerrelse);
    while n ~= 0
        dekryptert_blokk = mod(D*blokk, m);
        fwrite(dekryptert_fd, dekryptert_blokk);
        [blokk, n] = fread(kryptert_fd, blokkstoerrelse);
    end
    
    % 3. Lukk filene
    fclose(kryptert_fd);
    fclose(dekryptert_fd);
    
end