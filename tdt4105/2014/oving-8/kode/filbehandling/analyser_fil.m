function analyser_fil(filnavn)
    % 1. �pne filen
    fd = fopen(filnavn, 'r');
    
    % 2. Bruk filen
    antallLinjer = 0;
    antallOrd = 0;
    linje = fgetl(fd);
    while ischar(linje)
       antallLinjer = antallLinjer + 1;
       ordListe = strsplit(linje);
       antallOrd = antallOrd + length(ordListe);
       fprintf('%s\n', linje);
       linje = fgetl(fd);
    end
    fprintf('Antall linjer: %d\n', antallLinjer);
    fprintf('Antall ord:    %d\n', antallOrd);
    
    % 3. Lukk filen
    fclose(fd);
end