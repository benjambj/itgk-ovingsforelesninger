function krypter_fil(filnavn, kryptert_filnavn, dekrypterings_info_filnavn)
    %1. �pne filene
    % �pnes i bin�r-format for � unng� konvertering av tegn.
    fd = fopen(filnavn, 'rb'); 
    % output-fil m� definitivt �pnes i bin�r-format for � ikke konvertere
    % utskrift av newline-bokstaver.
    kryptert_fd = fopen(kryptert_filnavn, 'wb'); 
    
    %2. Bruk filene.
    [E, D, m] = lag_noekler();
    blokkstoerrelse = length(E);
    [blokk, n] = fread(fd, blokkstoerrelse);
    while n ~= 0
        if n < blokkstoerrelse
            blokk(end+1:blokkstoerrelse,1) = 0;
        end
       kryptert_blokk = mod(E * blokk, m);
       fwrite(kryptert_fd, kryptert_blokk);
       [blokk, n] = fread(fd, blokkstoerrelse);
    end
    save(dekrypterings_info_filnavn, 'D', 'm');
    
    % 3. Lukk filene
    fclose(fd);
    fclose(kryptert_fd);
end


