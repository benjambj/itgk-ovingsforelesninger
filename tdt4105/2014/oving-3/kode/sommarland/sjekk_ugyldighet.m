function sjekk_ugyldighet(hoyde, alder, funksjonshemmet, paa_nett, feilmelding)
    try
        pris = billettpris_sommarland(hoyde, alder, funksjonshemmet, paa_nett);
        gikk_bra = true;
    catch
        gikk_bra = false;
    end
    if gikk_bra
        error('%s.\nUtregnet pris ble %d', feilmelding, pris)
    end
end