function sjekk_pris(hoyde, alder, funksjonshemmet, paa_nett, forventet_pris, feilmelding)
    pris = billettpris_sommarland(hoyde, alder, funksjonshemmet, paa_nett);
    assert(pris == forventet_pris, '%s.\nForventet %d men prisen var %d', ...
        feilmelding, pris, forventet_pris);
end