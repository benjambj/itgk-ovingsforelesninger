function pris = billettpris_sommarland(hoyde, alder, funksjonshemmet, paa_nett)
    if hoyde < 95
        pris = 0;
    elseif funksjonshemmet && hoyde <= 140
        if paa_nett
            error('Man kan ikke f� funksjonshemmetrabatt for personer 95-140 cm p� nett');
        end
        pris = 220;
    elseif paa_nett
        pris = 249;
    elseif alder >= 62 || funksjonshemmet
        pris = 250;
    elseif hoyde <= 140
        pris = 289;
    else
        pris = 299;
    end
end