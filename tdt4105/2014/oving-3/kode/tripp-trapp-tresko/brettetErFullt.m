function fullt = brettetErFullt(brett, tomPlass)
    fullt = all(brett(:) ~= tomPlass);
end