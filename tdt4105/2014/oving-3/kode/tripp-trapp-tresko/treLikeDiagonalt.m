function like = treLikeDiagonalt(brett, brikke)
    like = treLike(diag(brett), brikke) || treLike(diag(flipud(brett)), brikke);
end