function [rad, kolonne] = lesInnRadOgKolonne()
    while true
       tekstlinje = input('Hvor vil du sette din brikke? ', 's');
       [tall, antall, feil] = sscanf(tekstlinje, '%d, %d');       
       if feil
           disp('Skriv inn koordinat p� form "<rad>, <kolonne>"');
           continue;
       end
       if antall ~= 2
           disp('For f� tall. Skriv inn koordinat p� form "<rad>, <kolonne>"');
           continue;
       end
       
       rad = tall(1);
       kolonne = tall(2);
       return;
    end
end