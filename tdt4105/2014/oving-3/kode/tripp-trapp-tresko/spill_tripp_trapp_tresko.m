tomPlass = '-';

brett = [ tomPlass, tomPlass, tomPlass; ...
          tomPlass, tomPlass, tomPlass; ...
          tomPlass, tomPlass, tomPlass]; % Kan bruke repmat(tomPlass, 3)
spilletPaagaar = true;
brikke = 'X';
while spilletPaagaar
   disp('Brettet s� langt:');
   disp(brett);
   [rad, kolonne] = hentNesteTrekk(brett, tomPlass, brikke);
   brett(rad, kolonne) = brikke;
   if harVunnet(brett, brikke)
       fprintf('Gratulerer, spiller %c vant!\n', brikke);
       spilletPaagaar = false;
   elseif brettetErFullt(brett, tomPlass)
       fprintf('Brettet er fylt opp, det ble uavgjort');
       spilletPaagaar = false;
   end
   if brikke == 'X'
       brikke = 'O';
   else
       brikke = 'X';
   end
end

disp('Brettet ved spillets slutt:');
disp(brett);