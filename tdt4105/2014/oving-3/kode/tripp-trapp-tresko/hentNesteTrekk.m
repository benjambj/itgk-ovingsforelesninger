function [rad, kolonne] = hentNesteTrekk(brett, tomPlass, brikke)
    fprintf('Det er spiller %c sin tur\n', brikke);
    manglerInput = true;
    while manglerInput
       [rad, kolonne] = lesInnRadOgKolonne();
       if brett(rad, kolonne) ~= tomPlass
           fprintf('P� plass (%d, %d) st�r brikke %c allerede\n', ...
               rad, kolonne, brett(rad, kolonne));
       else
           manglerInput = false;
       end
    end
end