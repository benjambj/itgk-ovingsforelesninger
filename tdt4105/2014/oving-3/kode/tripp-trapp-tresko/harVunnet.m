function vunnet = harVunnet(brett, brikke)
    if any(size(brett) ~= [3, 3])
        error('harVunnet fungerer bare p� 3x3 tripp-trapp-tresko-brett');
    end
    
    vunnet = treLikeBortover(brett, 1, brikke) || ...
        treLikeBortover(brett, 2, brikke) || ...
        treLikeBortover(brett, 3, brikke) || ...
        treLikeNedover(brett, 1, brikke) || ...
        treLikeNedover(brett, 2, brikke) || ...
        treLikeNedover(brett, 3, brikke) || ...
        treLikeDiagonalt(brett, brikke);
    
    
%% Alternative implementasjoner
%% Alternativ 1:
%    for n = 1:3
%        if treLikeBortover(brett, n, brikke) || treLikeNedover(brett, n, brikke)
%            vunnet = true;
%            return;
%        end
%    end
%    vunnet = treLikeDiagonalt(brett, brikke);

%% Alternativ 2:    
%    vunnet = any(arrayfun(@(n) treLikeBortover(brett, n, brikke) || ...
%                           treLikeNedover(brett, n, brikke), 1:3)) || ...
%                           treLikeDiagonalt(brett, brikke);

end