function result = treLike(rad, brikke)
    if length(rad) ~= 3
        error('Funksjon treLike forventer en rad med lengde tre');
    end
    result = all(rad == brikke);
end