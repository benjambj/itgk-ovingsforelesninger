function res = er_konsonant(streng)
    res = ismember(lower(streng), 'bcdfghjklmnpqrstvwxz');
end