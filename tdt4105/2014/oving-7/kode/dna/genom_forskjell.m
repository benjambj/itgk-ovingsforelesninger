%% genom_forskjell: antall operasjoner for � endre genom2 til genom 1
% Hvis man har lov til � enten legge til, fjerne eller endre bokstaver i
% genom2, hvor mange operasjoner m� man gj�re for � ende opp med genom1? 
% (Med disse operasjonene tillatt, kalles forskjellen ogs� for
% Levenshtein-distansen).
function avstand = genom_forskjell(genom1, genom2)
    avstander = zeros(length(genom1)+1, length(genom2)+1);
    avstander(1,:) = 0:length(genom2);
    avstander(:,1) = 0:length(genom1);
    for i = 2:length(genom1)+1
        for j = 2:length(genom2)+1
            if genom1(i-1) == genom2(j-1)
                avstander(i, j) = avstander(i-1, j-1);
            else
                legge_til = 1 + avstander(i, j-1);
                fjerne = 1 + avstander(i-1, j);
                endre = 1 + avstander(i-1, j-1);
                avstander(i, j) = min([legge_til, fjerne, endre]);
            end
        end
    end     
    avstand = avstander(end, end);
end