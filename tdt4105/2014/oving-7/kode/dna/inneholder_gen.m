function resultat = inneholder_gen(genom, gen)
    persistent impl;
    if isempty(impl)
       baser = 'ACGT';
       genom_stoerrelse = 1e6;
       gen_stoerrelse = 50;
       testgenom = baser(randi(numel(baser), 1, genom_stoerrelse));
       testgen = baser(randi(numel(baser), 1, gen_stoerrelse));
       impl = time_implementations({@inneholder_gen_strfind, ...
                                    @inneholder_gen_manuell, ...
                                    @inneholder_gen_kmp}, ...
                                    testgenom, testgen);
    end
    resultat = impl(genom, gen);
end

function resultat = inneholder_gen_strfind(genom, gen)
    resultat = ~isempty(strfind(genom, gen));
end

function resultat = inneholder_gen_manuell(genom, gen)
    gen_stoerrelse = length(gen);
    for i = 1:(length(genom)-gen_stoerrelse)
        if strncmp(genom(i:i+gen_stoerrelse), gen, gen_stoerrelse)
            resultat = true;
            return;
        end
    end
    resultat = false;
end

% En mer avansert funksjon, inneholder_gen_kmp, er plassert i en egen
% matlabfil. Den kan v�re interessant � studere, hvis man synes vanlig
% manuell strengs�k gj�r mye bortkastet arbeid.