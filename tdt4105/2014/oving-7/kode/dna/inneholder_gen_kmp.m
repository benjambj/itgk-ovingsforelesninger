%%
% Algoritmen under implementerer en mer avansert teksts�kealgoritme kalt
% KMP, etter dens oppfinnere Knuth, Morris, og Pratt. 
% En beskrivelse og forklaring av algoritmen finnes i denne filen, og p�
% en.wikipedia.org/wiki/Knuth-Morris-Pratt_algorithm
% 
% OBS: DENNE ALGORITMEN ER IKKE PENSUM. Ingen grunn til panikk :)
% 
% F�rst litt terminologi:
%    - S�kestreng: Det man s�ker etter
%    - Hovedstreng: Teksten man s�ker i.
%    - Prefiks: Start av streng, feks er 'a' og 'ab' prefikser av 'abc'
%
% Beskrivelse: 
%  Den enkleste m�ten � s�ke etter en s�kestreng i en hovedstreng er sjekke
%  hver posisjon i hovedstrengen, og se om s�kestrengen finnes der (som
%  implementert i funksjonen 'inneholder_gen_manuell' i filen
%  inneholder_gen.m). Dette medf�rer imidlertid mange un�dvendige
%  sammenlikninger, siden man potensielt ser p� hver bokstav i hovedstrengen
%  mange ganger. KMP-algoritmen pr�ver � bare unders�ke hver bokstav i
%  hovedstrengen �n gang, s� langt det lar seg gj�re.  
% 
% Eksempel: 
%  Anta at vi leter etter s�kestrengen 'aab' i hovedstrengen 'aaab'. Den
%  naive s�kealgoritmen vil gj�re f�lgende sammenlikninger:
%                      1.        2.        3.        4.        5.        6.
%      Hovedstreng: '<a>aab'  'a<a>ab'  'aa<a>b'  'a<a>ab'  'aa<a>b'  'aaa<b>'
%      S�kestreng:  '<a>ab'   'a<a>b'   'aa<b>'   '<a>ab'   'a<a>b'   'aa<b>' 
%
% KMP gj�r f�lgende sammenlikninger: 
%                      1.        2.        3.        4.        5.    
%      Hovedstreng: '<a>aab'  'a<a>ab'  'aa<a>b'  'aa<a>b'  'aaa<b>'
%      S�kestreng:  '<a>ab'   'a<a>b'   'aa<b>'   'a<a>b'   'aa<b>'
% 
% KMP fortsetter alts� s�ket p� det stedet i s�kestrengen hvor den vet at
% alt i s�kestrengen f�r denne posisjonen, alts� prefiksen frem til denne
% posisjonen, m� ha blitt matchet i s�kestrengen. Gitt s�kestrengen over,
% 'aab', vet man at hvis man finner en mismatch med 'b', har man f�tt
% en match med 'aa'. En match med 'aa' betyr at forrige bokstav var en 'a',
% og dette er starten p� s�kestrengen. Dette betyr at vi kan fortsette
% s�ket p� posisjon 2, siden bokstaven p� posisjon 1 ('a') m� v�re sett
% tidligere. 
% Et mer intuitivt eksempel, er s�kestrengen 'aksjeaksjer' (meta-aksjer!)
% Hvis vi matcher 'aksjeaksje' men s� f�r mismatch p� posisjon 11 (siste 
% bokstav), kan vi fortsette s�ket p� posisjon 6 (aksje-forekomst nr. 2)
% siden teksten kanskje matcher her (for eksempel hvis hovedstrengen var
% 'aksjeaksjeaksjer').

% Siden vi bruker KMP til � s�ke etter gen i genom, kaller vi hovedstrengen
% v�r for 'genom' og s�kestrengen v�r for 'gen'.
function resultat = inneholder_gen_kmp(genom, gen)
    prefiks_lengde = lag_prefiks_tabell(gen); % Fjern semikolon for � se hvordan prefikstabellen ser ut.
    gen_stoerrelse = length(gen);
    potensiell_match_start = 1;
    gen_indeks = 1;
    genom_indeks = 1;
    while genom_indeks <= length(genom)
    % Fjern kommentaren under for � f� utskrift over hvordan algoritmen
    % opererer.
    %    fprintf('Sjekker genom indeks %d og gen indeks %d\n', genom_indeks, gen_indeks);
        if genom(genom_indeks) == gen(gen_indeks)
            if gen_indeks == gen_stoerrelse % Alle bokstaver matcher
                resultat = genom_indeks - gen_stoerrelse + 1;
                return;
            end
            gen_indeks = gen_indeks + 1;
            genom_indeks = genom_indeks + 1;
        else
            if gen_indeks == 1
                % F�rste bokstav i genet stemmer ikke, g� videre i genomet.
                genom_indeks = genom_indeks + 1;
                potensiell_match_start = genom_indeks;
            else
                % Vi �nsker � fortsette s�ket p� det punkt i s�kestrengen 
                % hvor vi vet at alle tidligere bokstaver m� ha blitt
                % funnet i hovedstrengen.                 
                % Eksempel: Hvis v�rt gen er 'aataag' og gen_indeks er 6,
                % og vi fant 'aataa' i genomet men s� ikke en 'g', vil vi
                % fortsette s�ket fra posisjon 3 for � se om vi finner en
                % 't'.
                % Grafisk: 
                % (| indikerer potensiell match start i genom, og
                %     gen_indeks i gen)
                % (> indikerer genom_indeks)
                % Gitt situasjonen
                %    Genom: '.....|aataa>t...'
                %    Gen:   'aataa|g'
                %  �nsker vi � fortsette s�ket i situasjonen
                %    Genom: '.....aat|aa>t...'
                %    Gen:   'aa|taag'
                antall_gyldige_bokstaver = prefiks_lengde(gen_indeks);
                antall_ugyldige_bokstaver = gen_indeks - antall_gyldige_bokstaver - 1;
                potensiell_match_start = potensiell_match_start + antall_ugyldige_bokstaver;
                gen_indeks = gen_indeks - antall_ugyldige_bokstaver;
            end
        end
    end
    resultat = false;
end

% Funksjonen lag_prefiks_tabell preprosesserer s�kestrengen, for � finne ut
% hvilke hvor lang prefiks av s�kestrengen vi har matchet p� forskjellige
% posisjoner i s�kestrengen.
% P� en gitt posisjon, tenker vi:
%   'Bommer vi her, hvor mange bokstaver f�r oss finnes ogs� fra starten av
%   s�keteksten?'
%  Hver bokstav som ogs� finnes i starten av s�keteksten trenger vi ikke
%  sjekke p� nytt.
function prefiks_lengde = lag_prefiks_tabell(soekestreng)
    prefiks_lengde = zeros(1, length(soekestreng));
    prefiks_lengde(1) = 0;
    prefiks_lengde(2) = 0;
    prefiks_posisjon = 1;
    for i = 3:length(prefiks_lengde)
        while soekestreng(i-1) ~= soekestreng(prefiks_posisjon) && ...
                prefiks_posisjon > 1
            prefiks_posisjon = prefiks_lengde(prefiks_posisjon) + 1;            
        end
        if soekestreng(i-1) == soekestreng(prefiks_posisjon)
            prefiks_lengde(i) = prefiks_posisjon;
            prefiks_posisjon = prefiks_posisjon + 1;
        else
            prefiks_lengde(i) = 0;
        end
    end
end