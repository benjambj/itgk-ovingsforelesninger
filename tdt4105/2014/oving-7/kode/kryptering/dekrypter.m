function res = dekrypter(s, D, m)
    res = blanks(length(s));
    blokkstr = length(D);
    for i = 1:blokkstr:length(s)
        res(i:i+blokkstr-1) = mod(s(i:i+blokkstr-1) * D, m);
    end
end