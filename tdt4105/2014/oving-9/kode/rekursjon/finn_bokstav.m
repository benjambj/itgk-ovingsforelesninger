function resultat = finn_bokstav(streng, bokstav)
    if isempty(streng)
        resultat = false;
    elseif streng(1) == bokstav
        resultat = true;
    else
        resultat = finn_bokstav(streng(2:end), bokstav);
    end
end