function resultat = duplicates(x, y)
    if isempty(x)
        resultat = [];
    else
        resultat = duplicates(x(2:end), y);
        if any(y == x(1)) && all(resultat ~= x(1))
            resultat = [resultat, x(1)];
        end
    end
end