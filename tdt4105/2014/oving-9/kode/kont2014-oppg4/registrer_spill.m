function registrer_spill()
    results = hent_spill_fra_bruker()
    poeng = [0, 0];
    for i = 1:size(results, 1)
        omgang = results(i,:);
       if strcmp(omgang{1}, 'N/S')
           poeng(1) = poeng(1) + omgang{4};
           poeng(2) = poeng(2) + omgang{5};
       else
           poeng(2) = poeng(2) + omgang{4};
           poeng(1) = poeng(1) + omgang{5};
       end
    end
    fprintf('Total score:\n');
    fprintf(' N/S %d\n', poeng(1));
    fprintf(' �/V %d\n', poeng(2));
end

function spillTabell = hent_spill_fra_bruker()
    spillTabell = [];
    lag = input('Lag (N/S eller �/V, annet for � slutte): ', 's');
    while strcmp(lag, 'N/S') || strcmp(lag, '�/V')
        melding = input('Melding: ', 's');
        stikk = input('Stikk: ');
        poeng = bridgePoints(melding, stikk);
        if poeng < 0
            beitpoeng = -poeng;
            poeng = 0;
        else
            beitpoeng = 0;
        end
        spillTabell = [spillTabell; { lag, melding, stikk, poeng, beitpoeng}];
        lag = input('Lag (N/S eller �/V, annet for � slutte): ', 's');
    end
end