function resultat = bidOk(melding, antallStikk)
    trekk = melding(1) - '0';
    resultat = antallStikk >= trekk + 6;
end