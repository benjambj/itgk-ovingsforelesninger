# README #

Dette repoet inneholder øvingsforelesninger i ITGK ved NTNU, inkludert eksempelkode. Matlab-versjonene ligger i undermappen tdt4105, og python-versjonene i tdt4110.

Dersom foilsettet genereres av feks fra LaTeX eller ORG-mode filer, er den genererte filen (typisk PDF) beholdt i repoet slik at personer uten det nødvendige verktøyoppsettet likevel kan inspisere og gjenbruke dem.